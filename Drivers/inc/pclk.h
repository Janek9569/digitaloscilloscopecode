#ifndef INC_PCLK_HPP_
#define INC_PCLK_HPP_

#include <stdint.h>

void pclk_init();
uint8_t pclk_is_initialised();
void pclk_deinit();
void increment_freq();
void decrement_freq();


#endif

