#ifndef INC_ADC_H_
#define INC_ADC_H_

#include <stdint.h>

#define ADC_OWFA_PIN (GP(0))
#define ADC_OWFA_PORT (GPIOA)

#define ADC_OWFB_PIN (GP(3))
#define ADC_OWFB_PORT (GPIOH)

typedef enum _Adc_div
{
	ADC_DIV_1 = 1,
	ADC_DIV_2 = 2,
	ADC_DIV_4 = 4,
	ADC_DIV_LAST
}Adc_div;

void adc_init();
uint8_t adc_enable();
void adc_disable();
void adc_set_div(Adc_div new_div);
Adc_div adc_get_div();

#endif
