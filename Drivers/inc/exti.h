#ifndef INC_EXTI_H_
#define INC_EXTI_H_

#include <stdint.h>
#include "stm32f7xx_hal.h"

#define MAX_ATTACHED_CALLBACKS 5

typedef enum _Exti_type
{
	EXTI_0,
	EXTI_1,
	EXTI_2,
	EXTI_3,
	EXTI_4,
	EXTI_5,
	EXTI_6,
	EXTI_7,
	EXTI_8,
	EXTI_9,
	EXTI_10,
	EXTI_11,
	EXTI_12,
	EXTI_13,
	EXTI_14,
	EXTI_15,
	EXTI_TYPE_LAST
}Exti_type ;

void exti_init_prio(Exti_type type, uint32_t pin, GPIO_TypeDef* port, uint32_t pull, uint32_t speed, uint32_t mode, uint8_t prio);
void exti_init(Exti_type type, uint32_t pin, GPIO_TypeDef* port, uint32_t pull, uint32_t speed, uint32_t mode);
void exti_delete_callback_ISR(Exti_type type, void (*f) (void));
void exti_delete_callback(Exti_type type, void (*f) (void));
uint8_t exti_add_callback(Exti_type type, void (*f) (void));



#endif
