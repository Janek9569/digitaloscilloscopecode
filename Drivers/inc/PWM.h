#ifndef INC_PWM_H_
#define INC_PWM_H_

#include <stdint.h>

typedef enum _Registered_PWM
{
	PWM_LCD_ADJ,
	PWM_LAST
}Registered_PWM;

void PWM_init(Registered_PWM type);
void PWM_deinit(Registered_PWM type);
void PWM_start(Registered_PWM type);
void PWM_stop(Registered_PWM type);
void PWM_set_val(Registered_PWM type, uint32_t val);
uint32_t PWM_get_val(Registered_PWM type);

#endif
