#ifndef INC_SPI_H_
#define INC_SPI_H_

#include "stdint.h"

typedef enum _SPI_enumerator
{
	SPI1_EN,
	SPI2_EN,
	SPI5_EN,
	SPI_EN_LAST
}SPI_enumerator;

void spi_init(SPI_enumerator spi_en);
void spi_deinit(SPI_enumerator spi_en);
void spi_transmit(SPI_enumerator spi_en, uint8_t* data, uint32_t size);
void spi_recieve(SPI_enumerator spi_en, uint8_t* data, uint32_t size);
void spi__transmit(SPI_enumerator spi_en, uint8_t* data, uint32_t size);
void spi__recieve(SPI_enumerator spi_en, uint8_t* data, uint32_t size);


#endif
