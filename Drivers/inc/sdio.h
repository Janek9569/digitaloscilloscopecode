#ifndef INC_SDIO_H_
#define INC_SDIO_H_
#include <stdint.h>

void sdio_init();
void sdio_deinit();
uint8_t sdio_read();
uint8_t sdio_write();


#endif
