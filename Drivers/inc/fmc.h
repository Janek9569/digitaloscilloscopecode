#ifndef INC_FMC_H_
#define INC_FMC_H_

#include <stdint.h>

#define FMC_START_ADDR (0xC0000000)
#define TEST_SIZE 50000

void fmc_init();
void fmc_deinit();
uint8_t fmc_test();


#endif
