#ifndef INC_DAC_H_
#define INC_DAC_H_

#include <stdint.h>

typedef enum _Dac_en
{
	DAC_A,
	DAC_B,
	DAC_LAST
}Dac_en;

void dac_init();
void dac_deinit();
void dac_set(Dac_en type, uint32_t val);
void dac_start(Dac_en type);
void dac_stop(Dac_en type);
uint32_t dac_get_value(Dac_en type);

#endif
