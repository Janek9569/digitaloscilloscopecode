#ifndef INC_LTDC_H_
#define INC_LTDC_H_

#include <stdint.h>

void ltdc_init();
void ltdc_enable();
void ltdc_enable_backlight();
void ltdc_disable_backlight();
#endif
