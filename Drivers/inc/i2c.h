#ifndef INC_I2C_H_
#define INC_I2C_H_

#include <stdint.h>

void i2c_init();
void i2c_deinit();
void i2c_write_mem(uint16_t dev_addr, uint16_t mem_addr, uint16_t mem_size, uint8_t* data, uint16_t len);
void i2c_read_mem(uint16_t dev_addr, uint16_t mem_addr, uint16_t mem_size, uint8_t* data, uint16_t len);


#endif
