#ifndef INC_FPGA_COMMUNICATOR_H_
#define INC_FPGA_COMMUNICATOR_H_

#include <stdint.h>

#define SRAM_CAP (0x400000)//in bytes
#define SRAM_CAP_SAMPLES (SRAM_CAP / 2)

typedef enum _Fpga_comm_state
{
	FPGA_WAITING_FOR_INT,
	FPGA_READING,
	FPGA_IDLE,
	FPGA_LAST
}Fpga_comm_state;

typedef enum _Fulfill_alg_type
{
	FULFILL_ALG_ZERO,
	FULFILL_ALG_SCENE_WIDTH,
	FULFILL_ALG_LAST
}Fulfill_alg_type;

void fpga_set_WRITE();
void fpga_set_READ();
void fpga_communicator_init();
void fpga_communicator_deinit();
void fpga_set_data_ptr(uint32_t* data);
void fpga_set_read_cpl_callback( void (*f) (void));
void fpga_set_fulfill_alg(Fulfill_alg_type type);
Fpga_comm_state fpga_get_state();
uint8_t fpga_start_waiting_for_int();


#endif
