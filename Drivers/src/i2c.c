#include "i2c.h"
#include "global_utils.h"
#include "stm32f7xx_hal.h"
#include "FreeRTOS.h"
#include "semphr.h"

I2C_HandleTypeDef i2c_handle;
DMA_HandleTypeDef i2c_dma_rx;
DMA_HandleTypeDef i2c_dma_tx;
static SemaphoreHandle_t i2c_sem;

static uint8_t i2c_lock_sem(uint32_t timeout)
{
	return xSemaphoreTake( i2c_sem, timeout ) == pdTRUE;
}

static void i2c_release_sem()
{
	xSemaphoreGive(i2c_sem);
}

static void i2c_release_sem_ISR()
{
	static BaseType_t xHigherPriorityTaskWoken;
	xSemaphoreGiveFromISR(i2c_sem, &xHigherPriorityTaskWoken);
}

void i2c_init()
{
	static uint8_t was_initialised = false;
	if(was_initialised)
		return;

	i2c_handle.Instance = I2C1;
	i2c_handle.Init.Timing = 0x20404768;
	i2c_handle.Init.OwnAddress1 = 0;
	i2c_handle.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	i2c_handle.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	i2c_handle.Init.OwnAddress2 = 0;
	i2c_handle.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	i2c_handle.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	i2c_handle.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;

	ASSERT(HAL_I2C_Init(&i2c_handle) == HAL_OK);
	ASSERT(HAL_I2CEx_ConfigAnalogFilter(&i2c_handle, I2C_ANALOGFILTER_ENABLE) == HAL_OK);
	ASSERT(HAL_I2CEx_ConfigDigitalFilter(&i2c_handle, 0) == HAL_OK);

	i2c_sem = xSemaphoreCreateBinary();
	i2c_release_sem();
}

void HAL_I2C_MspInit(I2C_HandleTypeDef* i2cHandle)
{
	 GPIO_InitTypeDef GPIO_InitStruct;

	 GPIO_InitStruct.Pin = GP(6) | GP(7);
	 GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	 GPIO_InitStruct.Pull = GPIO_PULLUP;
	 GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	 GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
	 HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	 __HAL_RCC_I2C1_CLK_ENABLE();

	 i2c_dma_rx.Instance = DMA1_Stream0;
	 i2c_dma_rx.Init.Channel = DMA_CHANNEL_1;
	 i2c_dma_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
	 i2c_dma_rx.Init.PeriphInc = DMA_PINC_DISABLE;
	 i2c_dma_rx.Init.MemInc = DMA_MINC_ENABLE;
	 i2c_dma_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	 i2c_dma_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	 i2c_dma_rx.Init.Mode = DMA_NORMAL;
	 i2c_dma_rx.Init.Priority = DMA_PRIORITY_LOW;
	 i2c_dma_rx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;

	 ASSERT(HAL_DMA_Init(&i2c_dma_rx) == HAL_OK);
	 __HAL_LINKDMA(i2cHandle, hdmarx, i2c_dma_rx);

	 i2c_dma_tx.Instance = DMA1_Stream6;
	 i2c_dma_tx.Init.Channel = DMA_CHANNEL_1;
	 i2c_dma_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
	 i2c_dma_tx.Init.PeriphInc = DMA_PINC_DISABLE;
	 i2c_dma_tx.Init.MemInc = DMA_MINC_ENABLE;
	 i2c_dma_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	 i2c_dma_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	 i2c_dma_tx.Init.Mode = DMA_NORMAL;
	 i2c_dma_tx.Init.Priority = DMA_PRIORITY_LOW;
	 i2c_dma_tx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;

	 ASSERT(HAL_DMA_Init(&i2c_dma_tx) == HAL_OK);
	 __HAL_LINKDMA(i2cHandle, hdmatx, i2c_dma_tx);

	 HAL_NVIC_SetPriority(I2C1_EV_IRQn, 0, 0);
	 HAL_NVIC_EnableIRQ(I2C1_EV_IRQn);
	 HAL_NVIC_SetPriority(I2C1_ER_IRQn, 0, 0);
	 HAL_NVIC_EnableIRQ(I2C1_ER_IRQn);
}

void i2c_deinit()
{
	__HAL_RCC_I2C1_CLK_DISABLE();
	HAL_GPIO_DeInit(GPIOB, GP(6) | GP(7));
	HAL_NVIC_DisableIRQ(I2C1_EV_IRQn);
	HAL_NVIC_DisableIRQ(I2C1_ER_IRQn);

    HAL_DMA_DeInit(i2c_handle.hdmarx);
    HAL_DMA_DeInit(i2c_handle.hdmatx);
}

void i2c_write_mem(uint16_t dev_addr, uint16_t mem_addr, uint16_t mem_size, uint8_t* data, uint16_t len)
{
	i2c_lock_sem(WAIT_FOR_EVER);
	HAL_I2C_Mem_Write_IT(&i2c_handle, dev_addr, mem_addr, mem_size, data, len);
}

void i2c_read_mem(uint16_t dev_addr, uint16_t mem_addr, uint16_t mem_size, uint8_t* data, uint16_t len)
{
	i2c_lock_sem(WAIT_FOR_EVER);
	HAL_I2C_Mem_Read_IT(&i2c_handle, dev_addr, mem_addr, mem_size, data, len);

	i2c_lock_sem(WAIT_FOR_EVER);
	i2c_release_sem();
}

/*****INTS*****/

void I2C1_EV_IRQHandler(void)
{
	HAL_I2C_EV_IRQHandler(&i2c_handle);
	i2c_release_sem_ISR();
}

void I2C1_ER_IRQHandler(void)
{
	HAL_I2C_ER_IRQHandler(&i2c_handle);
}

void DMA1_Stream0_IRQHandler(void)
{
	HAL_DMA_IRQHandler(&i2c_dma_rx);
	i2c_release_sem_ISR();
}

void DMA1_Stream6_IRQHandler(void)
{
	HAL_DMA_IRQHandler(&i2c_dma_tx);
	i2c_release_sem_ISR();
}



