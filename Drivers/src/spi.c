#include "spi.h"
#include "stm32f7xx_hal.h"
#include "global_utils.h"
#include "FreeRTOS.h"
#include "semphr.h"

typedef struct _Spi
{
	SPI_TypeDef* instance;
	uint32_t direction;
	SPI_HandleTypeDef handle;
	uint8_t initialised;
	SemaphoreHandle_t xSemaphore;
	SPI_enumerator en;
}Spi;

static Spi __attribute__((used)) spi[SPI_EN_LAST] = { {.initialised = 0}, {.initialised = 0}, {.initialised = 0} };

static Spi* get_spi(SPI_enumerator en)
{
	return &spi[en];
}

static uint8_t spi_lock_sem(SPI_enumerator spi_en, uint32_t timeout)
{
	Spi* spi = get_spi(spi_en);
	return xSemaphoreTake( spi->xSemaphore, timeout ) == pdTRUE;
}

static void spi_release_sem(SPI_enumerator spi_en)
{
	Spi* spi = get_spi(spi_en);
	xSemaphoreGive(spi->xSemaphore);
}

static void spi_release_sem_ISR(SPI_enumerator spi_en)
{
	Spi* spi = get_spi(spi_en);
	static BaseType_t xHigherPriorityTaskWoken;
	xSemaphoreGiveFromISR(spi->xSemaphore, &xHigherPriorityTaskWoken);
}

void spi_init(SPI_enumerator spi_en)
{
	Spi* spi = get_spi(spi_en);
	ASSERT(spi->initialised == false);
	spi->en = spi_en;

	switch((uint32_t)spi_en)
	{
	case (uint32_t)SPI1_EN:
		spi->instance = SPI1;
		spi->direction = SPI_DIRECTION_2LINES;
		break;

	case (uint32_t)SPI2_EN:
		spi->instance = SPI2;
		spi->direction = SPI_DIRECTION_2LINES;
		break;

	case (uint32_t)SPI5_EN:
		spi->instance = SPI5;
		spi->direction = SPI_DIRECTION_2LINES_RXONLY;
		break;

	default:
		ERR_FATAL("This SPI is not supported in this device", NULL, NULL, NULL);
	}

		ASSERT( (spi->direction == SPI_DIRECTION_1LINE)  		||
				(spi->direction == SPI_DIRECTION_2LINES) 		||
				(spi->direction == SPI_DIRECTION_2LINES_RXONLY)	);

		spi->handle.Instance = spi->instance;
		spi->handle.Init.Mode = SPI_MODE_MASTER;
		spi->handle.Init.Direction = spi->direction;
		spi->handle.Init.DataSize = SPI_DATASIZE_8BIT;
		spi->handle.Init.CLKPolarity = SPI_POLARITY_LOW;
		spi->handle.Init.CLKPhase = SPI_PHASE_1EDGE;
		spi->handle.Init.NSS = SPI_NSS_SOFT;
		spi->handle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
		spi->handle.Init.FirstBit = SPI_FIRSTBIT_MSB;
		spi->handle.Init.TIMode = SPI_TIMODE_DISABLE;
		spi->handle.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
		spi->handle.Init.CRCPolynomial = 7;
		spi->handle.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
		spi->handle.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;

		ASSERT(HAL_SPI_Init(&spi->handle) == HAL_OK);

		spi->xSemaphore = xSemaphoreCreateBinary();
		ASSERT(spi->xSemaphore != NULL);
		spi_release_sem(spi_en);

		spi->initialised = true;
}

void spi_transmit(SPI_enumerator spi_en, uint8_t* data, uint32_t size)
{
	Spi* spi = get_spi(spi_en);
	spi_lock_sem(spi_en, WAIT_FOR_EVER);
	HAL_SPI_Transmit_IT(&spi->handle, data, size);
}

void spi_recieve(SPI_enumerator spi_en, uint8_t* data, uint32_t size)
{
	Spi* spi = get_spi(spi_en);
	spi_lock_sem(spi_en, WAIT_FOR_EVER);
	HAL_SPI_Receive_IT(&spi->handle, data, size);
	spi_lock_sem(spi_en, WAIT_FOR_EVER);
	spi_release_sem(spi_en);
}

void spi__transmit(SPI_enumerator spi_en, uint8_t* data, uint32_t size)
{
	Spi* spi = get_spi(spi_en);
	HAL_StatusTypeDef ref = HAL_SPI_Transmit(&spi->handle, data, size, 1000);
	(void) ref;
}

void spi__recieve(SPI_enumerator spi_en, uint8_t* data, uint32_t size)
{
	Spi* spi = get_spi(spi_en);
	HAL_StatusTypeDef ref = HAL_SPI_Receive(&spi->handle, data, size, 1000);
	(void) ref;
}

void HAL_SPI_MspInit(SPI_HandleTypeDef* spiHandle)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	if(spiHandle->Instance==SPI1)
	{
		__HAL_RCC_SPI1_CLK_ENABLE();
		GPIO_InitStruct.Pin = GP(3) | GP(4) | GP(5);
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

		HAL_NVIC_SetPriority(SPI1_IRQn, 5, 0);
		HAL_NVIC_EnableIRQ(SPI1_IRQn);
	}
	else if(spiHandle->Instance==SPI2)
	{
		__HAL_RCC_SPI2_CLK_ENABLE();
		GPIO_InitStruct.Pin = GP(3);
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
		HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = GP(13) | GP(14) ;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

		HAL_NVIC_SetPriority(SPI2_IRQn, 5, 0);
		HAL_NVIC_EnableIRQ(SPI2_IRQn);
	}
	else if(spiHandle->Instance==SPI5)
	{
		__HAL_RCC_SPI5_CLK_ENABLE();

		GPIO_InitStruct.Pin = GP(7);
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF5_SPI5;
		HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = GP(7);
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF5_SPI5;
		HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

		HAL_NVIC_SetPriority(SPI5_IRQn, 5, 0);
		HAL_NVIC_EnableIRQ(SPI5_IRQn);
	}
}

void HAL_SPI_MspDeInit(SPI_HandleTypeDef* spiHandle)
{
	if(spiHandle->Instance==SPI1)
	{
		__HAL_RCC_SPI1_CLK_DISABLE();
		HAL_GPIO_DeInit(GPIOB, GP(3) | GP(4) | GP(5));
		HAL_NVIC_DisableIRQ(SPI1_IRQn);
	}
	else if(spiHandle->Instance==SPI2)
	{
		__HAL_RCC_SPI2_CLK_DISABLE();
		HAL_GPIO_DeInit(GPIOC, GP(3));
		HAL_GPIO_DeInit(GPIOB, GP(13) | GP(14));
		HAL_NVIC_DisableIRQ(SPI2_IRQn);
	}
	else if(spiHandle->Instance==SPI5)
	{
		__HAL_RCC_SPI5_CLK_DISABLE();
		HAL_GPIO_DeInit(GPIOF, GP(7));
		HAL_GPIO_DeInit(GPIOH, GP(7));
		HAL_NVIC_DisableIRQ(SPI5_IRQn);
	}
}

/****** interrupts ******/
void SPI1_IRQHandler(void)
{
	Spi* spi = get_spi(SPI1_EN);
	HAL_SPI_IRQHandler(&spi->handle);
	spi_release_sem_ISR(SPI1_EN);
}

void SPI2_IRQHandler(void)
{
	Spi* spi = get_spi(SPI2_EN);
	HAL_SPI_IRQHandler(&spi->handle);
	spi_release_sem_ISR(SPI2_EN);
}

void SPI5_IRQHandler(void)
{
	Spi* spi = get_spi(SPI5_EN);
	HAL_SPI_IRQHandler(&spi->handle);
	spi_release_sem_ISR(SPI5_EN);
}






