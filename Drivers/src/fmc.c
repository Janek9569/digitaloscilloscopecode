#include "fmc.h"
#include "global_utils.h"
#include "stm32f7xx_hal.h"
#include "stm32f7xx_ll_fmc.h"

static SDRAM_HandleTypeDef handle;

#define SDRAM_MEMORY_WIDTH FMC_SDRAM_MEM_BUS_WIDTH_32
#define SDCLOCK_PERIOD     FMC_SDRAM_CLOCK_PERIOD_2
#define SDRAM_TIMEOUT      ((uint32_t)0xFFFF)
#define REFRESH_COUNT      ((uint32_t)0x0603)
#define SDRAM_MODEREG_BURST_LENGTH_1             ((uint16_t)0x0000)
#define SDRAM_MODEREG_BURST_LENGTH_2             ((uint16_t)0x0001)
#define SDRAM_MODEREG_BURST_LENGTH_4             ((uint16_t)0x0002)
#define SDRAM_MODEREG_BURST_LENGTH_8             ((uint16_t)0x0004)
#define SDRAM_MODEREG_BURST_TYPE_SEQUENTIAL      ((uint16_t)0x0000)
#define SDRAM_MODEREG_BURST_TYPE_INTERLEAVED     ((uint16_t)0x0008)
#define SDRAM_MODEREG_CAS_LATENCY_2              ((uint16_t)0x0020)
#define SDRAM_MODEREG_CAS_LATENCY_3              ((uint16_t)0x0030)
#define SDRAM_MODEREG_OPERATING_MODE_STANDARD    ((uint16_t)0x0000)
#define SDRAM_MODEREG_WRITEBURST_MODE_PROGRAMMED ((uint16_t)0x0000)
#define SDRAM_MODEREG_WRITEBURST_MODE_SINGLE     ((uint16_t)0x0200)

static void SD_ram_init();

typedef struct _Pin_cfg
{
	GPIO_TypeDef* port;
	uint32_t pin;
}Pin_cfg;

//D20(PH12), D21(PH13), D22(PH14), D23(PH15), D24(PI0), D25(PI1), D26(PI2), D27(PI3), D28(PI6), D29(PI7), D30(PI9), D31(PI10)
//D8(PE11), D9(PE12), D10(PE13), D11(PE14), D12(PE15), D13(PD8), D14(PD9), D15(PD10), D16(PH8), D17(PH9), D18(PH10), D19(PH11)
//D0(PD14), D1(PD15), D2(PD0), D3(PD1), D4(PE7), D5(PE8), D6(PE9), D7(PE10)
//A0(PF0), A1(PF1), A2(PF2), A3(PF3), A4(PF4), A5(PF5), A6(PF12), A7(PF13), A8(PF14), A9(PF15), A10(PG0), A11(PG1)
//FMC_SDNE0(PC2), FMC_SDCKE0(PH2), FMC_SDNWE(PH5), FMC_SDNRAS(PF11), FMC_BA0(PG4), FMC_BA1(PG5), FMC_SDCLK(PG8)
//FMC_SDNCAS(PG15), FMC_NBL0(PE0), FMC_NBL1(PE1), FMC_NBL2(PI4), FMC_NBL3(PI5)
void fmc_init()
{
	__HAL_RCC_FMC_CLK_ENABLE();
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF12_FMC;

	GPIO_InitStruct.Pin = GP(9) | GP(10) | GP(0) | GP(1) | GP(2) | GP(3) | GP(4) | GP(5) | GP(6) | GP(7);
	HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(0) | GP(1) | GP(2) | GP(3) | GP(4) | GP(5) | GP(11) | GP(12) | GP(13) | GP(14) | GP(15);
	HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(2);
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(2) | GP(5) | GP(8) | GP(9) | GP(10) | GP(11) | GP(12) | GP(13) | GP(14) | GP(15);
	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(0) | GP(1) | GP(4) | GP(5) | GP(8) | GP(15);
	HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(7) | GP(8) | GP(9) | GP(10) | GP(11) | GP(12) | GP(13) | GP(14) | GP(15) | GP(0) | GP(1);
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(8) | GP(9) | GP(10) | GP(14) | GP(15) | GP(0) | GP(1);
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	handle.Instance = FMC_SDRAM_DEVICE;
	handle.Init.SDBank = FMC_SDRAM_BANK1;
	handle.Init.ColumnBitsNumber = FMC_SDRAM_COLUMN_BITS_NUM_9;
	handle.Init.RowBitsNumber = FMC_SDRAM_ROW_BITS_NUM_12;
	handle.Init.MemoryDataWidth = FMC_SDRAM_MEM_BUS_WIDTH_16;
	handle.Init.InternalBankNumber = FMC_SDRAM_INTERN_BANKS_NUM_4;
	handle.Init.CASLatency = FMC_SDRAM_CAS_LATENCY_2;
	handle.Init.WriteProtection = FMC_SDRAM_WRITE_PROTECTION_DISABLE;
	handle.Init.SDClockPeriod = FMC_SDRAM_CLOCK_PERIOD_2;
	handle.Init.ReadBurst = FMC_SDRAM_RBURST_ENABLE;
	handle.Init.ReadPipeDelay = FMC_SDRAM_RPIPE_DELAY_1;

	FMC_SDRAM_TimingTypeDef timing;
	timing.LoadToActiveDelay = 2;
	timing.ExitSelfRefreshDelay = 8;
	timing.SelfRefreshTime = 5;
	timing.RowCycleDelay = 7;
	timing.WriteRecoveryTime = 2;
	timing.RPDelay = 3;
	timing.RCDDelay = 3;

	ASSERT(HAL_SDRAM_Init(&handle, &timing) == HAL_OK);
	SD_ram_init();

	ASSERT(fmc_test() == true);
}

void fmc_deinit()
{
	__HAL_RCC_FMC_CLK_DISABLE();
	HAL_GPIO_DeInit(GPIOI, GP(9) | GP(10) | GP(0) | GP(1) | GP(2) | GP(3) | GP(4) | GP(5) | GP(6) | GP(7));
	HAL_GPIO_DeInit(GPIOF, GP(0) | GP(1) | GP(2) | GP(3) | GP(4) | GP(5) | GP(11) | GP(12) | GP(13) | GP(14) | GP(15));
	HAL_GPIO_DeInit(GPIOC, GP(2));
	HAL_GPIO_DeInit(GPIOH, GP(2) | GP(5) | GP(8) | GP(9) | GP(10) | GP(11) | GP(12) | GP(13) | GP(14) | GP(15));
	HAL_GPIO_DeInit(GPIOG, GP(0) | GP(1) | GP(4) | GP(5) | GP(8) | GP(15));
	HAL_GPIO_DeInit(GPIOE, GP(7) | GP(8) | GP(9) | GP(10) | GP(11) | GP(12) | GP(13) | GP(14) | GP(15) | GP(0) | GP(1));
	HAL_GPIO_DeInit(GPIOD, GP(8) | GP(9) | GP(10) | GP(14) | GP(15) | GP(0) | GP(1));
}

uint8_t fmc_test()
{
	volatile uint32_t test_buffer[TEST_SIZE];
	volatile uint32_t* sdram_ptr = ((uint32_t*)FMC_START_ADDR);

	for(uint32_t i = 0; i < TEST_SIZE; ++i)
	{
		sdram_ptr[i] = 0xFFFFFFFFUL;
	}

	for(uint32_t i = 0; i < TEST_SIZE; ++i)
	{
		sdram_ptr[i] = i;
	}

	for(uint32_t i = 0; i < TEST_SIZE; ++i)
	{
		test_buffer[i] = sdram_ptr[i];
		if(test_buffer[i] != i)
			return false;
	}

	for(uint32_t i = 0; i < TEST_SIZE; ++i)
	{
		sdram_ptr[i] = 0xFFFFFFFFUL;
	}

	return true;
}

static void SD_ram_init()
{
	volatile uint32_t tmpmrd = 0;

	FMC_SDRAM_CommandTypeDef cmd;

	cmd.CommandMode = FMC_SDRAM_CMD_CLK_ENABLE;
	cmd.CommandTarget = FMC_SDRAM_CMD_TARGET_BANK1;
	cmd.AutoRefreshNumber = 1;
	cmd.ModeRegisterDefinition = 0;

	ASSERT(HAL_SDRAM_SendCommand(&handle, &cmd, 10000) == HAL_OK);

	HAL_Delay(10);

    cmd.CommandMode = FMC_SDRAM_CMD_PALL;
    cmd.CommandTarget = FMC_SDRAM_CMD_TARGET_BANK1;
    cmd.AutoRefreshNumber = 1;
    cmd.ModeRegisterDefinition = 0;
    ASSERT(HAL_SDRAM_SendCommand(&handle, &cmd, 10000) == HAL_OK);

    cmd.CommandMode = FMC_SDRAM_CMD_AUTOREFRESH_MODE;
    cmd.CommandTarget = FMC_SDRAM_CMD_TARGET_BANK1;
    cmd.AutoRefreshNumber = 4;
    cmd.ModeRegisterDefinition = 0;
    ASSERT(HAL_SDRAM_SendCommand(&handle, &cmd, 10000) == HAL_OK);

    tmpmrd = (uint32_t) SDRAM_MODEREG_BURST_LENGTH_1 |
                        SDRAM_MODEREG_BURST_TYPE_SEQUENTIAL |
                        SDRAM_MODEREG_CAS_LATENCY_2 |
                        SDRAM_MODEREG_OPERATING_MODE_STANDARD |
                        SDRAM_MODEREG_WRITEBURST_MODE_SINGLE;

    cmd.CommandMode = FMC_SDRAM_CMD_LOAD_MODE;
    cmd.CommandTarget = FMC_SDRAM_CMD_TARGET_BANK1;
    cmd.AutoRefreshNumber = 1;
    cmd.ModeRegisterDefinition = tmpmrd;
    ASSERT(HAL_SDRAM_SendCommand(&handle, &cmd, 10000) == HAL_OK);

    HAL_SDRAM_ProgramRefreshRate(&handle, 1667);
}

/*****INTS*****/
void FMC_IRQHandler(void)
{
	HAL_SDRAM_IRQHandler(&handle);
}
