#include <PCLK.h>
#include "global_utils.h"
#include <string.h>
#include "i2c.h"
#include "FreeRTOS.h"
#include "semphr.h"

#define DIV1_src_sel 3 // 0 - PLL1, 1- xtal
#define EN_CLKIN 1
#define I2C_addr_1 6
#define I2C_addr_0 5
#define PLL3_refin_sel 2
#define M_DIV_MASK (0x1F)
#define OUT_DIV3_MASK (0xF0)
#define OUT_DIV3_X_SHIFT (7)
#define OUT_DIV3_Y_SHIFT (5)
#define PLL3_PDB 5
#define PLL1_PDB 1
#define DIFF2_EN 4
#define SE1_OUTPUT_EN 7
#define SE3_OUTPUT_EN 4
#define SE2_OUTPUT_EN 7
#define DIFF1_CLK_SEL  7
#define DIFF1_OUTPUT_TYPE_1 5
#define DIFF1_OUTPUT_TYPE_2 4
#define DIFF1_AMP_1 3
#define DIFF1_AMP_2 2
#define DIVIDER_FEEDBACK_H_MASK 0x07
#define DEVICE_SET_ADDR 0

typedef enum _PCLK_reg
{
	GENERAL_CONTROL = 0,
	DASH_CODE_ID = 1,
	CRYSTAL_CAP_SETTING = 2,
	PLL3_M_DIVIDER = 3,
	PLL3_N_DIVIDER = 4,
	PLL3_LOOP_FILER_SETTING_AND_N_DIVIDER = 5,
	PLL3_CHARGE_PUMP_CONTROL = 6,
	PLL1_CONTROL_AND_OUTDIV5_DIVIDER = 7,
	PLL1_M_DIVIDER = 8,
	PLL1_VCO_N_DIVIDER = 9,
	PLL_LOOP_FILTERAND_N_DIVIDER = 10,
	PLL1_CHARGE_PUMP = 11,
	PLL1_SPREAD_SPECTRUM_CONTROL_1 = 12,
	PLL1_SPREAD_SPECTRUM_CONTROL_2 = 13,
	PLL1_SPREAD_SPECTRUM_CONTROL_3 = 14,
	OUTPUT_DIVIDER1_CONTROL = 15,
	PLL2_INTEGER_FEEDBACK_DIVIDER_1 = 16,
	PLL2_INTEGER_FEEDBACK_DIVIDER_2 = 17,
	PLL2_FRACTIONAL_FEEDBACK_DIVIDER_1 = 18,
	PLL2_FRACTIONAL_FEEDBACK_DIVIDER_2 = 19,
	PLL2_SPREAD_SPECTRUM_CONTROL_1 = 20,
	PLL2_SPREAD_SPECTRUM_CONTROL2 = 21,
	PLL2_SPREAD_SPECTRUM_CONTROL3 = 22,
	PLL2_PERIOD_CONTROL = 23,
	PLL2_CONTROL_REGISTER = 24,
	PLL2_CHARGE_PUMP_CONTROL = 25,
	PLL2_M_DIVIDER_SETTING = 26,
	OUTPUT_DIVIDER4 = 27,
	PLL_OPERATION_CONTROL_REGISTER = 28,
	OUTPUT_CONTROL = 29,
	OE_AND_DFC_CONTROL = 30,
	CONTROL_REGISTER_1 = 31,
	CONTROL_REGISTER_2 = 32,
	SE3_AND_DIFF1_CONTROL_REGISTER = 33,
	DIFF1_CONTROL_REGISTER = 34,
	DIFF2_CONTROL_REGISTER = 35,
	SE1_AND_DIV4_CONTROL = 36,
	PCLK_REG_LAST
}PCLK_reg;

typedef struct _Set
{
	uint8_t x;
	uint8_t y;
}Set;

static SemaphoreHandle_t pclk_sem;

static void pclk_release_sem()
{
	xSemaphoreGive(pclk_sem);
}

static uint8_t pclk_lock_sem(uint32_t timeout)
{
	return xSemaphoreTake( pclk_sem, timeout ) == pdTRUE;
}

static uint8_t pclk_addr = 0xD1;
static uint8_t __attribute__((used)) Divider_3_div_table[4][4] =
{
		{1, 	2, 		4, 		8 },
		{3, 	6, 		12, 	24},
		{5, 	10, 	20, 	40},
		{10, 	10, 	40, 	80}
};
static Set increasing_divider_3_set[] =
{
	{0,0},//1
	{1,0},//2
	{0,1},//3
	{2,0},//4
	{0,2},//5
	{1,1},//6
	{3,0},//8
	{0,3},//10
	{2,1},//12
	{2,2},//20
	{3,1},//24
	{3,2},//40
	//{3,3} //80
};

static int8_t actual_set_pos = 0;
static uint8_t pclk_regs[PCLK_REG_LAST];

static uint8_t __attribute__((used)) _read_reg(PCLK_reg reg)
{
	pclk_lock_sem(WAIT_FOR_EVER);
	uint8_t data_buffer = 0x00;
	i2c_read_mem(pclk_addr, reg, 1, &data_buffer, 1);
	pclk_release_sem();
	return data_buffer;
}

static void __attribute__((used)) _write_reg(PCLK_reg reg, uint8_t val)
{
	pclk_lock_sem(WAIT_FOR_EVER);
	i2c_write_mem(pclk_addr, reg, 1, &val, 1);
	pclk_release_sem();
}

static void __attribute__((used)) _read_all_regs()
{
	pclk_lock_sem(WAIT_FOR_EVER);
	i2c_read_mem(pclk_addr, 0, 1, pclk_regs, PCLK_REG_LAST);
	pclk_release_sem();
}

static uint8_t __attribute__((used)) read_reg(PCLK_reg reg, uint8_t val)
{
	pclk_lock_sem(WAIT_FOR_EVER);
	uint8_t data_buffer = 0x00;
	i2c_read_mem(pclk_addr, reg, 1, &data_buffer, 1);
	pclk_release_sem();
	return data_buffer;
}

static void __attribute__((used)) write_reg(PCLK_reg reg, uint8_t val)
{
	pclk_lock_sem(WAIT_FOR_EVER);
	i2c_write_mem(pclk_addr, reg, 1, &val, 1);
	pclk_regs[reg] = val;
	pclk_release_sem();
}


static void pclk_set_m_div(uint8_t val)
{
	pclk_lock_sem(WAIT_FOR_EVER);
	val = val & M_DIV_MASK;
	uint8_t act_reg_val = 0;
	i2c_read_mem(pclk_addr, PLL2_M_DIVIDER_SETTING, 1, &act_reg_val, 1);
	act_reg_val = (act_reg_val & (~M_DIV_MASK)) | val;
	i2c_write_mem(pclk_addr, PLL2_M_DIVIDER_SETTING, 1, &act_reg_val, 1);
	pclk_regs[PLL2_M_DIVIDER_SETTING] = act_reg_val;
	pclk_release_sem();
}

//<1:0> - y  <3:2> - x
static void pclk_set_out_div(uint8_t set_pos)
{
	pclk_lock_sem(WAIT_FOR_EVER);
	actual_set_pos = set_pos;
	const uint8_t x = increasing_divider_3_set[set_pos].x;
	const uint8_t y = increasing_divider_3_set[set_pos].y;

	uint8_t act_reg_val = 0;
	i2c_read_mem(pclk_addr, OUTPUT_DIVIDER4, 1, &act_reg_val, 1);
	act_reg_val = (act_reg_val & (~OUT_DIV3_MASK));
	act_reg_val |= (x << OUT_DIV3_X_SHIFT);
	act_reg_val |= (y << OUT_DIV3_Y_SHIFT);
	i2c_write_mem(pclk_addr, OUTPUT_DIVIDER4, 1, &act_reg_val, 1);
	pclk_regs[OUTPUT_DIVIDER4] = act_reg_val;
	pclk_release_sem();
}

static void pclk_set_feedback_div(uint16_t val)
{
	pclk_lock_sem(WAIT_FOR_EVER);
	uint8_t H_val = (val >> 8) & DIVIDER_FEEDBACK_H_MASK;
	uint8_t L_val = (uint8_t)(val & 0xFF00);

	uint8_t act_reg_L = 0;
	uint8_t act_reg_H = 0;
	i2c_read_mem(pclk_addr, PLL2_INTEGER_FEEDBACK_DIVIDER_1, 1, &act_reg_H, 1);
	act_reg_L = L_val;
	act_reg_H = H_val;
	i2c_write_mem(pclk_addr, PLL2_INTEGER_FEEDBACK_DIVIDER_1, 1, &act_reg_H, 1);
	i2c_write_mem(pclk_addr, PLL2_INTEGER_FEEDBACK_DIVIDER_2, 1, &act_reg_L, 1);
	pclk_regs[PLL2_INTEGER_FEEDBACK_DIVIDER_1] = act_reg_H;
	pclk_regs[PLL2_INTEGER_FEEDBACK_DIVIDER_2] = act_reg_L;
	pclk_release_sem();
}

//PLL2 + DIVIDER 3 -> B34<7>(1) B0<3>(0)
//set VCO output to 192 MHz
static uint8_t pclk_was_initialised = false;
void pclk_init()
{

	ASSERT(pclk_was_initialised == false);
	++pclk_was_initialised;

	pclk_sem = xSemaphoreCreateBinary();
	pclk_release_sem();

	i2c_init();

	_read_all_regs();

	uint8_t temp_reg = pclk_regs[GENERAL_CONTROL];

//set_cfg_bits
	//set device address
	temp_reg &= ~( (1 << I2C_addr_1) | (1 << I2C_addr_0) );
	temp_reg |= (DEVICE_SET_ADDR & ( (1 << 1) | (1 << 0) ) << 6);
	//set Divider1 source to XTAL
	temp_reg &= ~(1 << DIV1_src_sel);
	//set pll3 refin to XTAL
	temp_reg &= ~(1 << PLL3_refin_sel);
	//enable CLKIN
	temp_reg |= (1 << EN_CLKIN);
	i2c_write_mem(pclk_addr, GENERAL_CONTROL, 1, &temp_reg, 1);
	pclk_regs[GENERAL_CONTROL] = temp_reg;

//set_m_divider
	pclk_set_m_div(25);// PLL Fin := 1MHz

//set feedback div to 192
	pclk_set_feedback_div(192);//VCO is 192MHz

//set_out_div
	pclk_set_out_div(0);// PLL fout without division

//disable PLL3, PLL1
	temp_reg = pclk_regs[PLL_OPERATION_CONTROL_REGISTER];
	temp_reg &= ~(1 << PLL3_PDB);
	temp_reg &= ~(1 << PLL1_PDB);
	i2c_write_mem(pclk_addr, PLL_OPERATION_CONTROL_REGISTER, 1, &temp_reg, 1);
	pclk_regs[PLL_OPERATION_CONTROL_REGISTER] = temp_reg;

//disable diff 2 clok output
	temp_reg = pclk_regs[OUTPUT_CONTROL];
	temp_reg &= ~(1 << DIFF2_EN);
	i2c_write_mem(pclk_addr, OUTPUT_CONTROL, 1, &temp_reg, 1);
	pclk_regs[OUTPUT_CONTROL] = temp_reg;

//disable SE1, SE3 output clock
	temp_reg = pclk_regs[OE_AND_DFC_CONTROL];
	temp_reg &= ~(1 << SE1_OUTPUT_EN);
	temp_reg &= ~(1 << SE3_OUTPUT_EN);
	i2c_write_mem(pclk_addr, OE_AND_DFC_CONTROL, 1, &temp_reg, 1);
	pclk_regs[OE_AND_DFC_CONTROL] = temp_reg;

//disable SE2 output clock
	temp_reg = pclk_regs[CONTROL_REGISTER_2];
	temp_reg &= ~(1 << SE2_OUTPUT_EN);
	i2c_write_mem(pclk_addr, CONTROL_REGISTER_2, 1, &temp_reg, 1);
	pclk_regs[CONTROL_REGISTER_2] = temp_reg;

//attach div3 to PLL2, select LCPECL output - common at 2V
	temp_reg = pclk_regs[DIFF1_CONTROL_REGISTER];
	//attach div3
	temp_reg |= (1 << DIFF1_CLK_SEL);
	//select LVPECL
	temp_reg |= (1 << DIFF1_OUTPUT_TYPE_1);
	temp_reg &= ~(0 << DIFF1_OUTPUT_TYPE_2);
	//920mV diff amp
	temp_reg |= (1 << DIFF1_AMP_1);
	temp_reg |= (1 << DIFF1_AMP_2);
	i2c_write_mem(pclk_addr, DIFF1_CONTROL_REGISTER, 1, &temp_reg, 1);
	pclk_regs[DIFF1_CONTROL_REGISTER] = temp_reg;
}

uint8_t pclk_is_initialised()
{
	return pclk_was_initialised;
}

void pclk_deinit(){}

void increment_freq()
{
	pclk_lock_sem(WAIT_FOR_EVER);
	++actual_set_pos;
	if(actual_set_pos >= sizeof(increasing_divider_3_set))
	{
		pclk_release_sem();
		return;
	}
	pclk_set_out_div(actual_set_pos);
	pclk_release_sem();
}

void decrement_freq()
{
	pclk_lock_sem(WAIT_FOR_EVER);
	if(actual_set_pos == 0)
	{
		pclk_release_sem();
		return;
	}

	--actual_set_pos;
	pclk_set_out_div(actual_set_pos);
	pclk_release_sem();
}


















