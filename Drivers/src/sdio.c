#include "diskio.h"
#include "sdio.h"
#include "stm32f7xx_hal.h"
#include "global_utils.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include <string.h>

SD_HandleTypeDef sdmmc_handle;
DMA_HandleTypeDef sd_dma_rx;
DMA_HandleTypeDef sd_dma_tx;
static SemaphoreHandle_t sd_sem;

static uint8_t sdmmc_lock_sem(uint32_t timeout)
{
	return xSemaphoreTake( sd_sem, timeout ) == pdTRUE;
}

static void sdmmc_release_sem()
{
	xSemaphoreGive(sd_sem);
}

static void sdmmc_release_sem_ISR()
{
	static BaseType_t xHigherPriorityTaskWoken;
	xSemaphoreGiveFromISR(sd_sem, &xHigherPriorityTaskWoken);
}

static void sdmmc_init()
{
	static uint8_t is_initialised = false;
	ASSERT(is_initialised == false);
	++is_initialised;

	sdmmc_handle.Instance = SDMMC1;
	sdmmc_handle.Init.ClockEdge = SDMMC_CLOCK_EDGE_RISING;
	sdmmc_handle.Init.ClockBypass = SDMMC_CLOCK_BYPASS_DISABLE;
	sdmmc_handle.Init.ClockPowerSave = SDMMC_CLOCK_POWER_SAVE_DISABLE;
	sdmmc_handle.Init.BusWide = SDMMC_BUS_WIDE_1B;
	sdmmc_handle.Init.HardwareFlowControl = SDMMC_HARDWARE_FLOW_CONTROL_DISABLE;
	sdmmc_handle.Init.ClockDiv = 0;

	ASSERT(HAL_SD_Init(&sdmmc_handle) == HAL_OK);
	ASSERT(HAL_SD_ConfigWideBusOperation(&sdmmc_handle, SDMMC_BUS_WIDE_4B) == HAL_OK);


	sd_sem = xSemaphoreCreateBinary();
	sdmmc_release_sem();
}

static void sdmmc_deinit()
{
	__HAL_RCC_SDMMC1_CLK_DISABLE();

	HAL_GPIO_DeInit(GPIOC, GP(8) | GP(9) | GP(10) | GP(11) | GP(12));
	HAL_GPIO_DeInit(GPIOD, GPIO_PIN_2);
	HAL_NVIC_DisableIRQ(SDMMC1_IRQn);
}

void HAL_SD_MspInit(SD_HandleTypeDef* sdHandle)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	__HAL_RCC_SDMMC1_CLK_ENABLE();
    __HAL_RCC_DMA2_CLK_ENABLE();

	GPIO_InitStruct.Pin = GP(8) | GP(9) | GP(10) | GP(11) | GP(12);
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF12_SDMMC1;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GP(2);
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF12_SDMMC1;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

    sd_dma_rx.Instance = DMA2_Stream3;
    sd_dma_rx.Init.Channel = DMA_CHANNEL_4;
    sd_dma_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
    sd_dma_rx.Init.PeriphInc = DMA_PINC_DISABLE;
    sd_dma_rx.Init.MemInc = DMA_MINC_ENABLE;
    sd_dma_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
    sd_dma_rx.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
    sd_dma_rx.Init.Mode = DMA_PFCTRL;
    sd_dma_rx.Init.Priority = DMA_PRIORITY_LOW;
    sd_dma_rx.Init.FIFOMode = DMA_FIFOMODE_ENABLE;
    sd_dma_rx.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_FULL;
    sd_dma_rx.Init.MemBurst = DMA_MBURST_INC4;
    sd_dma_rx.Init.PeriphBurst = DMA_PBURST_INC4;
    ASSERT(HAL_DMA_Init(&sd_dma_rx) == HAL_OK);

    __HAL_LINKDMA(sdHandle, hdmarx, sd_dma_rx);

    sd_dma_tx.Instance = DMA2_Stream6;
    sd_dma_tx.Init.Channel = DMA_CHANNEL_4;
    sd_dma_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
    sd_dma_tx.Init.PeriphInc = DMA_PINC_DISABLE;
    sd_dma_tx.Init.MemInc = DMA_MINC_ENABLE;
    sd_dma_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
    sd_dma_tx.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
    sd_dma_tx.Init.Mode = DMA_PFCTRL;
    sd_dma_tx.Init.Priority = DMA_PRIORITY_LOW;
    sd_dma_tx.Init.FIFOMode = DMA_FIFOMODE_ENABLE;
    sd_dma_tx.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_FULL;
    sd_dma_tx.Init.MemBurst = DMA_MBURST_INC4;
    sd_dma_tx.Init.PeriphBurst = DMA_PBURST_INC4;
    ASSERT(HAL_DMA_Init(&sd_dma_tx) == HAL_OK);

    __HAL_LINKDMA(sdHandle, hdmatx, sd_dma_tx);

    HAL_NVIC_SetPriority(DMA2_Stream3_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA2_Stream3_IRQn);

    HAL_NVIC_SetPriority(DMA2_Stream6_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA2_Stream6_IRQn);

    HAL_NVIC_SetPriority(SDMMC1_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(SDMMC1_IRQn);
}

void sdio_init()
{
	sdmmc_init();
}

void sdio_deinit()
{
	sdmmc_deinit();
    HAL_DMA_DeInit(&sd_dma_rx);
    HAL_DMA_DeInit(&sd_dma_tx);
}

uint8_t sdio_read(uint32_t drv, uint8_t*buff, uint32_t block, uint32_t count)
{
	while(HAL_SD_GetCardState(&sdmmc_handle) != HAL_SD_CARD_TRANSFER)
	{
		vTaskDelay(1);
	}

	if(!sdmmc_lock_sem(20000))
		return false;

	uint8_t status = HAL_SD_ReadBlocks_DMA(&sdmmc_handle, buff, block, count) == HAL_OK;

	if(!sdmmc_lock_sem(20000))
		return false;

	sdmmc_release_sem();
	return status;
}

uint8_t sdio_write(uint32_t drv, uint8_t*buff, uint32_t block, uint32_t count)
{
	static uint8_t test_buffer[5000];
	while(HAL_SD_GetCardState(&sdmmc_handle) != HAL_SD_CARD_TRANSFER)
	{
		vTaskDelay(1);
	}

	if(!sdmmc_lock_sem(20000))
		return false;

	uint8_t status = HAL_SD_WriteBlocks_DMA(&sdmmc_handle, buff, block, count) == HAL_OK;

	memset(test_buffer, 0x00, 5000);
	sdio_read(drv, test_buffer, block, 1);
	return status;
}

/*****INTS*****/
void SDMMC1_IRQHandler(void)
{
	HAL_SD_IRQHandler(&sdmmc_handle);
	sdmmc_release_sem_ISR();
}

void DMA2_Stream3_IRQHandler(void)
{
	HAL_DMA_IRQHandler(&sd_dma_rx);
	sdmmc_release_sem_ISR();
}

void DMA2_Stream6_IRQHandler(void)
{
	HAL_DMA_IRQHandler(&sd_dma_tx);
	sdmmc_release_sem_ISR();
}
