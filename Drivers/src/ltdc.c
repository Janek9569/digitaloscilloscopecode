#include "ltdc.h"
#include "global_utils.h"
#include "stm32f7xx_hal.h"
#include "fmc.h"
#include "PWM.h"

extern DMA2D_HandleTypeDef dma_handle;

LTDC_HandleTypeDef ltdc_handle;

#define  LTDC_BACKLIGHT_PIN 	GP(8)
#define  LTDC_BACKLIGHT_PORT 	GPIOF

#define LCD_PIXEL				LCD_X_SIZE * LCD_Y_SIZE
#define LCD_FRAME_BUFFER		FMC_START_ADDR
#define LCD_FRAME_OFFSET		(uint32_t)LCD_PIXEL * 3

#define LCD_X_SIZE     1024	          /* LCD Horizontal display area */
#define LCD_Y_SIZE     600		        /* LCD Vertical display area */

#define LCD_DCLK       58000000       /* LCD DCLK 34M */

#define H_VD           LCD_X_SIZE     /* LCD Horizontal display area */
#define H_PW           10             /* HSYNC pulse width */
#define H_BP           100             /* Horizontal back porch */
#define H_FP           210            /* Horizontal front porch */

#define V_VD			     LCD_Y_SIZE     /* LCD Vertical display area */
#define V_PW           10             /* VSYNC pulse width */
#define V_BP           10             /* Vertical back porch */
#define V_FP           18             /* Vertical front porch */

void ltdc_init()
{
	LTDC_LayerCfgTypeDef pLayerCfg;

	ltdc_handle.Instance = LTDC;
	ltdc_handle.Init.HSPolarity = LTDC_HSPOLARITY_AL;
	ltdc_handle.Init.VSPolarity = LTDC_VSPOLARITY_AL;
	ltdc_handle.Init.DEPolarity = LTDC_DEPOLARITY_AL;
	ltdc_handle.Init.PCPolarity = LTDC_PCPOLARITY_IPC;
	ltdc_handle.Init.Backcolor.Blue = 255;
	ltdc_handle.Init.Backcolor.Green = 0;
	ltdc_handle.Init.Backcolor.Red = 0;
	ltdc_handle.Init.HorizontalSync = H_PW - 1;
	ltdc_handle.Init.VerticalSync = V_PW - 1;
	ltdc_handle.Init.AccumulatedHBP = H_BP - 1;
	ltdc_handle.Init.AccumulatedVBP = V_BP - 1;
	ltdc_handle.Init.AccumulatedActiveW = H_VD + H_BP - 1;
	ltdc_handle.Init.AccumulatedActiveH = V_VD + V_BP - 1;
	ltdc_handle.Init.TotalWidth = H_VD + H_BP + H_FP - 1;
	ltdc_handle.Init.TotalHeigh = V_VD + V_BP + V_FP - 1;
	ASSERT(HAL_LTDC_Init(&ltdc_handle) == HAL_OK);

	pLayerCfg.WindowX0 = H_BP;
	pLayerCfg.WindowX1 = (H_VD + H_BP - 1);
	pLayerCfg.WindowY0 = V_BP;
	pLayerCfg.WindowY1 = (V_VD + V_BP - 1);
	pLayerCfg.PixelFormat = LTDC_PIXEL_FORMAT_RGB888;
	pLayerCfg.Alpha = 255;
	pLayerCfg.Alpha0 = 0;
	pLayerCfg.BlendingFactor1 = LTDC_BLENDING_FACTOR1_CA;
	pLayerCfg.BlendingFactor2 = LTDC_BLENDING_FACTOR2_CA;
	pLayerCfg.FBStartAdress = LCD_FRAME_BUFFER;
	pLayerCfg.ImageWidth = 1024;
	pLayerCfg.ImageHeight = 600;
	pLayerCfg.Backcolor.Blue = 255;
	pLayerCfg.Backcolor.Green = 0;
	pLayerCfg.Backcolor.Red = 0;

	ASSERT(HAL_LTDC_ConfigLayer(&ltdc_handle, &pLayerCfg, 0) == HAL_OK);

	pLayerCfg.FBStartAdress = LCD_FRAME_BUFFER + LCD_FRAME_OFFSET;
	pLayerCfg.BlendingFactor1 = LTDC_BLENDING_FACTOR1_PAxCA;
	pLayerCfg.BlendingFactor2 = LTDC_BLENDING_FACTOR2_PAxCA;
	ASSERT(HAL_LTDC_ConfigLayer(&ltdc_handle, &pLayerCfg, 1) == HAL_OK);

	HAL_LTDC_Reload(&ltdc_handle, LTDC_RELOAD_IMMEDIATE);

	ltdc_enable();

}

void ltdc_enable()
{
	HAL_LTDC_MspInit(&ltdc_handle);

	//backlight
	HAL_Delay(1);

	//backlight adj
	PWM_init(PWM_LCD_ADJ);
	PWM_set_val(PWM_LCD_ADJ, 50);
	PWM_start(PWM_LCD_ADJ);

	HAL_Delay(15);

	HAL_GPIO_WritePin(LTDC_BACKLIGHT_PORT, LTDC_BACKLIGHT_PIN, GPIO_PIN_RESET);

	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = LTDC_BACKLIGHT_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	HAL_GPIO_Init(LTDC_BACKLIGHT_PORT, &GPIO_InitStruct);

	HAL_GPIO_WritePin(LTDC_BACKLIGHT_PORT, LTDC_BACKLIGHT_PIN, GPIO_PIN_SET);
}

// B0(PE4), B1(PJ13), B2(PD6), B3(PJ15), B4(PG12), B5(PA3), B6(PK5), B7(PK6),
// G0(PE5), G1(PE6), G2(PA6), G3(PG10), G4(PB10), G5(PK0), G6(PC7), G7(PD3),
// R0(PI15), R1(PA2), R2(PA1), R3(PJ2), R4(PJ3), R5(PJ4), R6(PJ5), R7(PJ6),
// HSYNC(PI12), VSYNC(PI13), CLK(PI14), DE(PF10)
void HAL_LTDC_MspInit(LTDC_HandleTypeDef* ltdcHandle)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	__HAL_RCC_LTDC_CLK_ENABLE();

	GPIO_InitStruct.Pin = GP(4) | GP(5) | GP(6);
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF14_LTDC;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(12) | GP(13) | GP(14) | GP(15);
	HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(10);
	HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(1) | GP(2) | GP(3) | GP(6);
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(2) | GP(3) | GP(4) | GP(5) | GP(6) | GP(13) | GP(15);
	HAL_GPIO_Init(GPIOJ, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(10);
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(0) | GP(5) | GP(6);
	HAL_GPIO_Init(GPIOK, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(7);
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(3) | GP(6);
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(10) | GP(12);
	GPIO_InitStruct.Alternate = GPIO_AF9_LTDC;
	HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

	HAL_NVIC_SetPriority(LTDC_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(LTDC_IRQn);
	HAL_NVIC_SetPriority(LTDC_ER_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(LTDC_ER_IRQn);
}

void HAL_LTDC_MspDeInit(LTDC_HandleTypeDef* ltdcHandle)
{
	if(ltdcHandle->Instance==LTDC)
	{
		__HAL_RCC_LTDC_CLK_DISABLE();

		HAL_GPIO_DeInit(GPIOE, GP(4) | GP(5) | GP(6) );
		HAL_GPIO_DeInit(GPIOI, GP(12) | GP(13) | GP(14) | GP(15) );
		HAL_GPIO_DeInit(GPIOF, GP(10) );
		HAL_GPIO_DeInit(GPIOA, GP(1) | GP(2) | GP(3) | GP(6) );
		HAL_GPIO_DeInit(GPIOJ, GP(2) | GP(3) | GP(4) | GP(5) | GP(6) | GP(13) | GP(15) );
		HAL_GPIO_DeInit(GPIOB, GP(10));
		HAL_GPIO_DeInit(GPIOK, GP(0) | GP(5) | GP(6) );
		HAL_GPIO_DeInit(GPIOC, GP(7) );
		HAL_GPIO_DeInit(GPIOD, GP(3) | GP(6) );
		HAL_GPIO_DeInit(GPIOG, GP(10) | GP(12) );
		HAL_NVIC_DisableIRQ(LTDC_IRQn);
		HAL_NVIC_DisableIRQ(LTDC_ER_IRQn);
	}
}

void ltdc_enable_backlight()
{
	HAL_GPIO_WritePin(LTDC_BACKLIGHT_PORT, LTDC_BACKLIGHT_PIN, GPIO_PIN_SET);
}

void ltdc_disable_backlight()
{
	HAL_GPIO_WritePin(LTDC_BACKLIGHT_PORT, LTDC_BACKLIGHT_PIN, GPIO_PIN_RESET);
}

/*****INTS*****/
void LTDC_IRQHandler(void)
{
	HAL_LTDC_IRQHandler(&ltdc_handle);
}

void LTDC_ER_IRQHandler(void)
{
	HAL_LTDC_IRQHandler(&ltdc_handle);
}
