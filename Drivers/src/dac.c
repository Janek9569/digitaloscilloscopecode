#include "dac.h"
#include "stm32f7xx_hal.h"
#include "global_utils.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

static DAC_HandleTypeDef dac_handle;
static uint32_t output_val[DAC_LAST];
static SemaphoreHandle_t dac_semaphore;

static uint8_t dac_lock_sem(uint32_t timeout)
{
	return xSemaphoreTake( dac_semaphore, timeout ) == pdTRUE;
}

static void dac_release_sem()
{
	xSemaphoreGive(dac_semaphore);
}

void dac_init()
{
	static uint8_t dac_was_initialised = false;
	ASSERT(dac_was_initialised == false);

	DAC_ChannelConfTypeDef sConfig;
	dac_handle.Instance = DAC;
	ASSERT(HAL_DAC_Init(&dac_handle) == HAL_OK);

	sConfig.DAC_Trigger = DAC_TRIGGER_NONE;
	sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
	ASSERT(HAL_DAC_ConfigChannel(&dac_handle, &sConfig, DAC_CHANNEL_1) == HAL_OK);
	ASSERT(HAL_DAC_ConfigChannel(&dac_handle, &sConfig, DAC_CHANNEL_2) == HAL_OK);

	dac_semaphore = xSemaphoreCreateBinary();
	dac_release_sem();

	dac_set(DAC_A, 0);
	dac_set(DAC_B, 0);
	dac_start(DAC_A);
	dac_start(DAC_B);
}

void dac_deinit()
{
	if(dac_handle.Instance == DAC)
	{
		__HAL_RCC_DAC_CLK_DISABLE();
		HAL_GPIO_DeInit(GPIOA, GP(4)|GP(5));
		HAL_NVIC_DisableIRQ(TIM6_DAC_IRQn);
	}
}

void dac_set(Dac_en type, uint32_t val)
{
	ASSERT(type < DAC_LAST);

	dac_lock_sem(WAIT_FOR_EVER);
	output_val[type] = val;

	uint32_t _channel = type == DAC_A ? DAC_CHANNEL_2 : DAC_CHANNEL_1;
	HAL_DAC_SetValue(&dac_handle, _channel, DAC_ALIGN_8B_R, val);
	dac_release_sem();
}

void dac_start(Dac_en type)
{
	ASSERT(type < DAC_LAST);
	dac_lock_sem(WAIT_FOR_EVER);
	uint32_t _channel = type == DAC_A ? DAC_CHANNEL_2 : DAC_CHANNEL_1;
	HAL_DAC_Start(&dac_handle, _channel);
	dac_release_sem();
}

void dac_stop(Dac_en type)
{
	ASSERT(type < DAC_LAST);
	dac_lock_sem(WAIT_FOR_EVER);
	uint32_t _channel = type == DAC_A ? DAC_CHANNEL_2 : DAC_CHANNEL_1;
	HAL_DAC_Stop(&dac_handle, _channel);
	dac_release_sem();
}

uint32_t dac_get_value(Dac_en type)
{
	ASSERT(type < DAC_LAST);
	dac_lock_sem(WAIT_FOR_EVER);
	return output_val[type];
	dac_release_sem();
}

void HAL_DAC_MspInit(DAC_HandleTypeDef* dacHandle)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	if(dacHandle->Instance==DAC)
	{
		__HAL_RCC_DAC_CLK_ENABLE();

		GPIO_InitStruct.Pin = GPIO_PIN_4|GPIO_PIN_5;
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

		HAL_NVIC_SetPriority(TIM6_DAC_IRQn, 5, 0);
		HAL_NVIC_EnableIRQ(TIM6_DAC_IRQn);
	}
}

/*****INTS*****/
void TIM6_DAC_IRQHandler(void)
{
	HAL_DAC_IRQHandler(&dac_handle);
}
