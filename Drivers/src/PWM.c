#include "PWM.h"
#include "stm32f7xx_hal.h"
#include "global_utils.h"
#include "FreeRTOS.h"
#include "semphr.h"

void HAL_TIM_MspPostInit(TIM_HandleTypeDef* timHandle);

typedef struct _PWM_info
{
	TIM_TypeDef* instance;
	TIM_HandleTypeDef handle;
	uint32_t act_val;
	uint8_t is_initialised;
	uint32_t channel;
	uint32_t prescaler;
	uint32_t counter_mode;
	uint32_t period;
	uint32_t clock_div;
}PWM_info;

static SemaphoreHandle_t pwm_sem;

static PWM_info PWM_infos[PWM_LAST] =
{
	{//ok500hz //fill 0-100 //base 108MHz
		.instance = TIM14,
		.act_val = 0,
		.is_initialised = false,
		.channel = TIM_CHANNEL_1,
		.prescaler = 499,
		.counter_mode = TIM_COUNTERMODE_UP,
		.period = 99,
		.clock_div = TIM_CLOCKDIVISION_DIV4
	}
};

static void pwm_release_sem()
{
	xSemaphoreGive(pwm_sem);
}

static uint8_t pwm_lock_sem(uint32_t timeout)
{
	return xSemaphoreTake( pwm_sem, timeout ) == pdTRUE;
}

void PWM_init(Registered_PWM type)
{
	ASSERT(type != PWM_LAST);
	ASSERT(PWM_infos[type].is_initialised == false);

	pwm_sem = xSemaphoreCreateBinary();
	pwm_release_sem();

	TIM_HandleTypeDef* act_handle = &PWM_infos[type].handle;
	PWM_infos[type].act_val = 0;
	PWM_infos[type].is_initialised = true;

	TIM_OC_InitTypeDef sConfigOC;
	act_handle->Instance = PWM_infos[type].instance;
	act_handle->Init.Prescaler = PWM_infos[type].prescaler;
	act_handle->Init.CounterMode = PWM_infos[type].counter_mode;
	act_handle->Init.Period = PWM_infos[type].period;
	act_handle->Init.ClockDivision = PWM_infos[type].clock_div;
	act_handle->Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	ASSERT(HAL_TIM_Base_Init(act_handle) == HAL_OK);
	ASSERT(HAL_TIM_PWM_Init(act_handle) == HAL_OK);

	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = 0;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	ASSERT(HAL_TIM_PWM_ConfigChannel(act_handle, &sConfigOC, PWM_infos[type].channel) == HAL_OK);

	HAL_TIM_MspPostInit(act_handle);
}

void PWM_deinit(Registered_PWM type)
{
	ASSERT(type != PWM_LAST);

	PWM_infos[type].is_initialised = false;
	switch(type)
	{
	case PWM_LCD_ADJ:
		vSemaphoreDelete(pwm_sem);
		PWM_infos[type].act_val = 0;
		PWM_infos[type].is_initialised = false;
	    __HAL_RCC_TIM14_CLK_DISABLE();
	    HAL_NVIC_DisableIRQ(TIM8_TRG_COM_TIM14_IRQn);
		break;
	default:
		break;
	}
}

void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* tim_baseHandle)
{
	if(tim_baseHandle->Instance==TIM14)
	{
		__HAL_RCC_TIM14_CLK_ENABLE();

		HAL_NVIC_SetPriority(TIM8_TRG_COM_TIM14_IRQn, 5, 0);
		HAL_NVIC_EnableIRQ(TIM8_TRG_COM_TIM14_IRQn);
	}
}

void HAL_TIM_MspPostInit(TIM_HandleTypeDef* timHandle)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	if(timHandle->Instance==TIM14)
	{
		GPIO_InitStruct.Pin = GP(9);
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    	GPIO_InitStruct.Alternate = GPIO_AF9_TIM14;
    	HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);
	}
}

void PWM_start(Registered_PWM type)
{
	pwm_lock_sem(WAIT_FOR_EVER);
	ASSERT(type != PWM_LAST);
	ASSERT(PWM_infos[type].is_initialised == true);

	TIM_HandleTypeDef* act_handle = &PWM_infos[type].handle;
	uint32_t act_channel = PWM_infos[type].channel;
	HAL_TIM_Base_Start(act_handle);
	HAL_TIM_PWM_Start(act_handle, act_channel);
	pwm_release_sem();
}

void PWM_stop(Registered_PWM type)
{
	pwm_lock_sem(WAIT_FOR_EVER);
	ASSERT(type != PWM_LAST);
	ASSERT(PWM_infos[type].is_initialised == true);

	TIM_HandleTypeDef* act_handle = &PWM_infos[type].handle;
	uint32_t act_channel = PWM_infos[type].channel;
	HAL_TIM_Base_Stop(act_handle);
	HAL_TIM_PWM_Stop(act_handle, act_channel);
	pwm_release_sem();
}

void PWM_set_val(Registered_PWM type, uint32_t val)
{
	pwm_lock_sem(WAIT_FOR_EVER);
	ASSERT(type != PWM_LAST);
	ASSERT(PWM_infos[type].is_initialised == true);

	PWM_infos[type].act_val = val;

	uint32_t act_channel = PWM_infos[type].channel;
	TIM_TypeDef* act_instance = PWM_infos[type].instance;

	switch(act_channel)
	{
	case TIM_CHANNEL_1:
		act_instance->CCR1 = val;
		break;
	case TIM_CHANNEL_2:
		act_instance->CCR2 = val;
		break;
	case TIM_CHANNEL_3:
		act_instance->CCR3 = val;
		break;
	case TIM_CHANNEL_4:
		act_instance->CCR4 = val;
		break;
	case TIM_CHANNEL_5:
		act_instance->CCR5 = val;
		break;
	case TIM_CHANNEL_6:
		act_instance->CCR6 = val;
		break;
	default:
		break;
	}
	pwm_release_sem();
}

uint32_t PWM_get_val(Registered_PWM type)
{
	ASSERT(type != PWM_LAST);

	return PWM_infos[type].act_val;
}

/******INTS*****/
void TIM8_TRG_COM_TIM14_IRQHandler(void)
{
	HAL_TIM_IRQHandler(&PWM_infos[PWM_LCD_ADJ].handle);
}
