#include "fpga_communicator.h"
#include "stm32f7xx_hal.h"
#include "global_utils.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "exti.h"
#include "string.h"

static Fpga_comm_state act_state = FPGA_LAST;
static uint32_t* data_ptr = NULL;
static void (*read_cpl_callback) (void) = NULL;
static SemaphoreHandle_t fpga_deamon_semaphr;
static Fulfill_alg_type act_alg = FULFILL_ALG_ZERO;

static void fpga_read_deamon_handler(void* arg);
static void fpga_exti_signal();

static void fpga_set_reset()
{
	HAL_GPIO_WritePin(GPIOB, GP(1), GPIO_PIN_SET);
}

static void fpga_reset_reset()
{
	HAL_GPIO_WritePin(GPIOB, GP(1), GPIO_PIN_RESET);
}

static void fpga_tick()
{
	HAL_GPIO_WritePin(GPIOA, GP(7), GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOA, GP(7), GPIO_PIN_RESET);
}

static void fpga_set_write_enable()
{
	HAL_GPIO_WritePin(GPIOB, GP(0), GPIO_PIN_SET);
}

static void fpga_reset_write_enable()
{
	HAL_GPIO_WritePin(GPIOB, GP(0), GPIO_PIN_RESET);
}

static void fpga_release_deamon_sem()
{
	xSemaphoreGive(fpga_deamon_semaphr);
}

static void fpga_release_deamon_sem_ISR()
{
	static BaseType_t xHigherPriorityTaskWoken;
	xSemaphoreGiveFromISR(fpga_deamon_semaphr, &xHigherPriorityTaskWoken);
}

static uint8_t fpga_lock_deamon_sem()
{
	return xSemaphoreTake( fpga_deamon_semaphr, WAIT_FOR_EVER ) == pdTRUE;
}

void fpga_set_fulfill_alg(Fulfill_alg_type type)
{
	ASSERT(type < FULFILL_ALG_LAST);

	act_alg = type;
}

void fpga_set_WRITE()
{
	fpga_set_reset();
	fpga_set_write_enable();
	fpga_tick();
	fpga_tick();
	fpga_tick();
	fpga_reset_reset();
	act_state = FPGA_WAITING_FOR_INT;
}

void fpga_set_READ()
{
	fpga_set_reset();
	fpga_reset_write_enable();
	fpga_tick();
	fpga_tick();
	fpga_tick();
	fpga_reset_reset();
	for(uint8_t i = 0; i < 8; ++i)
		fpga_tick();
}


static void fpga_read_sram(Fulfill_alg_type type)
{
	uint8_t nibble_val = 0;
	uint32_t* p = data_ptr;

	if(!p)
		return;

	fpga_set_READ();

	for(uint32_t i = 0; i < 100 ; ++i)// by entries
	{
		p[i] = 0;
		nibble_val = 0;
		for(uint8_t n = 0; n < 8; ++n)// by uint16_t nibbles
		{
			fpga_tick();
			uint8_t d0_val = HAL_GPIO_ReadPin(GPIOA, GP(9)) == GPIO_PIN_SET ? 1 : 0;
			uint8_t d1_val = HAL_GPIO_ReadPin(GPIOA, GP(12)) == GPIO_PIN_SET ? 1 : 0;
			uint8_t d2_val = HAL_GPIO_ReadPin(GPIOA, GP(15)) == GPIO_PIN_SET ? 1 : 0;
			uint8_t d3_val = HAL_GPIO_ReadPin(GPIOD, GP(4)) == GPIO_PIN_SET ? 1 : 0;
			nibble_val = d0_val | (d1_val << 1) | (d2_val << 2) | (d3_val << 3);

			p[i] |= (nibble_val << (n*4));
		}
	}
}

//D0(), D1(), D2(), D3()
//data_rdy(), reset(), SRAM_sel(), clk()
void fpga_communicator_init()
{
	ASSERT(act_state == FPGA_LAST);
	act_state = FPGA_IDLE;
	data_ptr = NULL;
	read_cpl_callback = NULL;

	GPIO_InitTypeDef GPIO_InitStruct;
	//init D0-D3 pins
	GPIO_InitStruct.Pin = GP(4);
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(15) | GP(12) | GP(9);
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);


	exti_init(EXTI_1, GP(1), GPIOJ, GPIO_NOPULL, GPIO_SPEED_FREQ_VERY_HIGH, GPIO_MODE_IT_RISING);
	exti_add_callback(EXTI_1, fpga_exti_signal);

	//reset, SRAM_sel, clk
	HAL_GPIO_WritePin(GPIOB, GP(1), GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOB, GP(0), GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, GP(7), GPIO_PIN_RESET);

	GPIO_InitStruct.Pin = GP(0) | GP(1);
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(7);
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	fpga_deamon_semaphr = xSemaphoreCreateBinary();

	//fpga_read_sram();

	TaskHandle_t xHandle = NULL;
	ASSERT(xTaskCreate(fpga_read_deamon_handler, "FPGA_Deamon", 256, (void*)0, 0, &xHandle) == pdPASS);
}

void fpga_communicator_deinit()
{
	HAL_GPIO_DeInit(GPIOD, GP(4));
	HAL_GPIO_DeInit(GPIOA, GP(15) | GP (12) | GP(9) | GP(7));
	HAL_GPIO_DeInit(GPIOB, GP(0) | GP(1));

	exti_delete_callback(EXTI_1, fpga_exti_signal);
}

void fpga_set_data_ptr(uint32_t* data)
{
	data_ptr = data;
}

void fpga_set_read_cpl_callback( void (*f) (void) )
{
	read_cpl_callback = f;
}

Fpga_comm_state fpga_get_state()
{
	return act_state;
}

uint8_t fpga_start_waiting_for_int()
{
	if(act_state == FPGA_IDLE)
	{
		fpga_set_write_enable();
		fpga_tick();
		fpga_set_reset();
		fpga_reset_reset();
		act_state = FPGA_WAITING_FOR_INT;
		return 1;
	}
	else
	{
		return 0;
	}
}

/*****INTS*****/
static void fpga_exti_signal()
{
	if(act_state == FPGA_WAITING_FOR_INT)
	{
		act_state = FPGA_READING;
		fpga_release_deamon_sem_ISR();
	}
}


/***** RESOURCE DEAMONS *****/
static void fpga_read_deamon_handler(void* arg)
{
	for(;;)
	{
		fpga_lock_deamon_sem();

		fpga_read_sram(act_alg);

		act_state = FPGA_IDLE;

		if(read_cpl_callback)
			read_cpl_callback();
	}
}






