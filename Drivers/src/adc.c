#include "adc.h"
#include "stm32f7xx_hal.h"
#include "global_utils.h"
#include "exti.h"
#include "pclk.h"
#include "FreeRTOS.h"
#include "semphr.h"

#define ADC_SD_PIN (GP(4))
#define ADC_SD_PORT (GPIOH)

#define ADC_OWFA_PIN (GP(0))
#define ADC_OWFA_PORT (GPIOA)

#define ADC_OWFB_PIN (GP(3))
#define ADC_OWFB_PORT (GPIOH)

#define ADC_DIV_2_PIN (GP(4))
#define ADC_DIV_2_PORT (GPIOC)

#define ADC_DIV_4_PIN (GP(5))
#define ADC_DIV_4_PORT (GPIOC)

#define ADC_DISABLED_STATE (GPIO_PIN_SET)
#define ADC_ENABLED_STATE (GPIO_PIN_RESET)

static void adc_OFW_callback();
static Adc_div act_div = ADC_DIV_LAST;
static SemaphoreHandle_t adc_sem;

static void adc_release_sem()
{
	xSemaphoreGive(adc_sem);
}

static uint8_t adc_lock_sem(uint32_t timeout)
{
	return xSemaphoreTake( adc_sem, timeout ) == pdTRUE;
}

void adc_init()
{
	//ASSERT(pclk_is_initialised() == true);
	GPIO_InitTypeDef GPIO_InitStruct;

	HAL_GPIO_WritePin(ADC_SD_PORT, ADC_SD_PIN, ADC_DISABLED_STATE);
	GPIO_InitStruct.Pin = ADC_SD_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(ADC_SD_PORT, &GPIO_InitStruct);

	HAL_GPIO_WritePin(ADC_DIV_2_PORT, ADC_DIV_2_PIN, GPIO_PIN_RESET);
	GPIO_InitStruct.Pin = ADC_DIV_2_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(ADC_DIV_2_PORT, &GPIO_InitStruct);

	HAL_GPIO_WritePin(ADC_DIV_4_PORT, ADC_DIV_4_PIN, ADC_DISABLED_STATE);
	GPIO_InitStruct.Pin = ADC_DIV_4_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(ADC_DIV_4_PORT, &GPIO_InitStruct);

	adc_get_div(ADC_DIV_1);

	//ADC_B_OWF PA0
	exti_init_prio(EXTI_0, ADC_OWFA_PIN, ADC_OWFA_PORT, GPIO_PULLDOWN, GPIO_SPEED_FREQ_VERY_HIGH, GPIO_MODE_IT_RISING, 0);
	exti_add_callback(EXTI_0, adc_OFW_callback);

	//ADC_A_OWF PH3
	exti_init_prio(EXTI_3, ADC_OWFB_PIN, ADC_OWFB_PORT, GPIO_PULLDOWN, GPIO_SPEED_FREQ_VERY_HIGH, GPIO_MODE_IT_RISING, 0);
	exti_add_callback(EXTI_3, adc_OFW_callback);

	adc_sem = xSemaphoreCreateBinary();
	adc_release_sem();
}

static uint8_t _adc_is_OWF()
{
	if( (HAL_GPIO_ReadPin(ADC_OWFA_PORT, ADC_OWFA_PIN) == GPIO_PIN_SET) ||
		(HAL_GPIO_ReadPin(ADC_OWFB_PORT, ADC_OWFB_PIN) == GPIO_PIN_SET)    )
	{
		return true;
	}
	return false;
}

uint8_t adc_is_OWF()
{
	adc_lock_sem(WAIT_FOR_EVER);
	uint8_t status = _adc_is_OWF();
	adc_release_sem();
	return status;
}


void adc_disable()
{
	HAL_GPIO_WritePin(ADC_SD_PORT, ADC_SD_PIN, ADC_DISABLED_STATE);
}

void adc_set_div(Adc_div new_div)
{
	adc_lock_sem(WAIT_FOR_EVER);
	ASSERT(new_div != ADC_DIV_LAST);

	switch(new_div)
	{
	case ADC_DIV_1:
		HAL_GPIO_WritePin(ADC_DIV_2_PORT, ADC_DIV_2_PIN, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(ADC_DIV_4_PORT, ADC_DIV_2_PIN, GPIO_PIN_RESET);
		break;

	case ADC_DIV_2:
		HAL_GPIO_WritePin(ADC_DIV_2_PORT, ADC_DIV_2_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(ADC_DIV_4_PORT, ADC_DIV_2_PIN, GPIO_PIN_RESET);
		break;

	case ADC_DIV_4:
		HAL_GPIO_WritePin(ADC_DIV_2_PORT, ADC_DIV_2_PIN, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(ADC_DIV_4_PORT, ADC_DIV_2_PIN, GPIO_PIN_SET);
		break;

	default:
		break;
	}

	act_div = new_div;
	adc_release_sem();
}

Adc_div adc_get_div()
{
	return act_div;
}

uint8_t adc_enable()
{
	adc_lock_sem(WAIT_FOR_EVER);
	if(!_adc_is_OWF())
	{
		HAL_GPIO_WritePin(ADC_SD_PORT, ADC_SD_PIN, ADC_ENABLED_STATE);
		adc_release_sem();
		return true;
	}

	adc_disable();
	adc_release_sem();
	return false;
}

/***** CALLBACKS *****/
static void adc_OFW_callback()
{
	adc_disable();
}




