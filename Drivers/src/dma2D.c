#include "dma2D.h"
#include "stm32_dma2d.h"
#include "stm32f7xx_hal.h"
#include "global_utils.h"
DMA2D_HandleTypeDef dma_handle;

void dma2D_init()
{
	dma_handle.Instance = DMA2D;
	dma_handle.Init.Mode = DMA2D_M2M_BLEND;
	dma_handle.Init.ColorMode = DMA2D_OUTPUT_ARGB8888;
	dma_handle.Init.OutputOffset = 0;
	dma_handle.LayerCfg[1].InputOffset = 0;
	dma_handle.LayerCfg[1].InputColorMode = DMA2D_INPUT_ARGB8888;
	dma_handle.LayerCfg[1].AlphaMode = DMA2D_REPLACE_ALPHA;
	dma_handle.LayerCfg[1].InputAlpha = 0;
	dma_handle.LayerCfg[0].InputOffset = 0;
	dma_handle.LayerCfg[0].InputColorMode = DMA2D_INPUT_ARGB8888;
	dma_handle.LayerCfg[0].AlphaMode = DMA2D_REPLACE_ALPHA;
	dma_handle.LayerCfg[0].InputAlpha = 0;
	ASSERT(HAL_DMA2D_Init(&dma_handle) == HAL_OK);
	ASSERT(HAL_DMA2D_ConfigLayer(&dma_handle, 0) == HAL_OK);
	ASSERT(HAL_DMA2D_ConfigLayer(&dma_handle, 1) == HAL_OK);
}

void HAL_DMA2D_MspInit(DMA2D_HandleTypeDef* dma2dHandle)
{
	__HAL_RCC_DMA2D_CLK_ENABLE();
    HAL_NVIC_SetPriority(DMA2D_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(DMA2D_IRQn);
}

void dms2D_deinit()
{
	__HAL_RCC_DMA2D_CLK_DISABLE();
	HAL_NVIC_DisableIRQ(DMA2D_IRQn);
}

/*****INTS*****/
void DMA2D_IRQHandler(void)
{

}
