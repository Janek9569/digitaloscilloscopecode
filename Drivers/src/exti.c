#include "exti.h"
#include "global_utils.h"
#include "stm32f7xx_hal.h"
#include "stm32f7xx_hal_gpio.h"
#include "FreeRTOS.h"
#include "semphr.h"

static IRQn_Type irqn_table[EXTI_TYPE_LAST] =
{
	EXTI0_IRQn,
	EXTI1_IRQn,
	EXTI2_IRQn,
	EXTI3_IRQn,
	EXTI4_IRQn,
	EXTI9_5_IRQn,
	EXTI9_5_IRQn,
	EXTI9_5_IRQn,
	EXTI9_5_IRQn,
	EXTI9_5_IRQn,
	EXTI15_10_IRQn,
	EXTI15_10_IRQn,
	EXTI15_10_IRQn,
	EXTI15_10_IRQn,
	EXTI15_10_IRQn,
	EXTI15_10_IRQn
};

static uint8_t was_exti_initialised[EXTI_TYPE_LAST];
static uint32_t __attribute__((used)) exti_pin[EXTI_TYPE_LAST];
static GPIO_TypeDef* __attribute__((used)) exti_port[EXTI_TYPE_LAST];
static SemaphoreHandle_t exti_sem[EXTI_TYPE_LAST];

static void(*registered_callbacks[EXTI_TYPE_LAST][MAX_ATTACHED_CALLBACKS])(void);

static void exti_release_sem(Exti_type type)
{
	xSemaphoreGive(exti_sem[type]);
}

static uint8_t exti_lock_sem(Exti_type type, uint32_t timeout)
{
	return xSemaphoreTake( exti_sem[type], timeout ) == pdTRUE;
}

void exti_init(Exti_type type, uint32_t pin, GPIO_TypeDef* port, uint32_t pull, uint32_t speed, uint32_t mode)
{
	exti_init_prio(type, pin, port, pull, speed, mode, 10);
}

void exti_init_prio(Exti_type type, uint32_t pin, GPIO_TypeDef* port, uint32_t pull, uint32_t speed, uint32_t mode, uint8_t prio)
{
	ASSERT(pin <= GPIO_PIN_15);
	if(was_exti_initialised[type])
		return;

	exti_sem[type] = xSemaphoreCreateBinary();
	exti_release_sem(type);

	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = pin;
	GPIO_InitStruct.Mode = mode;
	GPIO_InitStruct.Pull = pull;
	HAL_GPIO_Init(port, &GPIO_InitStruct);

	HAL_NVIC_SetPriority(irqn_table[type], prio, 0);
	HAL_NVIC_EnableIRQ(irqn_table[type]);
}

void exti_delete_callback_ISR(Exti_type type, void (*f) (void))
{
	for(uint8_t i = 0; i < MAX_ATTACHED_CALLBACKS; ++i)
	{
		if(registered_callbacks[type][i] == f)
		{
			registered_callbacks[type][i] = NULL;
		}
	}
}

void exti_delete_callback(Exti_type type, void (*f) (void))
{
	exti_lock_sem(type, WAIT_FOR_EVER);
	for(uint8_t i = 0; i < MAX_ATTACHED_CALLBACKS; ++i)
	{
		if(registered_callbacks[type][i] == f)
		{
			registered_callbacks[type][i] = NULL;
		}
	}
	exti_release_sem(type);
}

uint8_t exti_add_callback(Exti_type type, void (*f) (void))
{
	exti_lock_sem(type, WAIT_FOR_EVER);
	for(uint8_t i = 0; i < MAX_ATTACHED_CALLBACKS; ++i)
	{
		if(registered_callbacks[type][i] == f)
		{
			exti_release_sem(type);
			return true;
		}
		if(registered_callbacks[type][i] == NULL)
		{
			registered_callbacks[type][i] = f;
			exti_release_sem(type);
			return true;
		}
	}
	uint8_t state = false;
	ASSERT(state == true);
	exti_release_sem(type);
	return false;
}

static void exti_call_callbacks(Exti_type type)
{
	for(uint8_t i = 0; i < MAX_ATTACHED_CALLBACKS; ++i)
	{
		if(registered_callbacks[type][i] != NULL)
		{
			registered_callbacks[type][i]();
		}
	}
}

/***** INTS *****/
void EXTI0_IRQHandler(void)
{
	if(__HAL_GPIO_EXTI_GET_IT(GP(0)) != RESET)
	{
	    __HAL_GPIO_EXTI_CLEAR_IT(GP(0));
		exti_call_callbacks(EXTI_0);
	}
}

void EXTI1_IRQHandler(void)
{
	if(__HAL_GPIO_EXTI_GET_IT(GP(1)) != RESET)
	{
	    __HAL_GPIO_EXTI_CLEAR_IT(GP(1));
	    exti_call_callbacks(EXTI_1);
	}
}

void EXTI2_IRQHandler(void)
{
	if(__HAL_GPIO_EXTI_GET_IT(GP(2)) != RESET)
	{
	    __HAL_GPIO_EXTI_CLEAR_IT(GP(2));
	    exti_call_callbacks(EXTI_2);
	}
}

void EXTI3_IRQHandler(void)
{
	if(__HAL_GPIO_EXTI_GET_IT(GP(3)) != RESET)
	{
	    __HAL_GPIO_EXTI_CLEAR_IT(GP(3));
	    exti_call_callbacks(EXTI_3);
	}
}

void EXTI4_IRQHandler(void)
{
	if(__HAL_GPIO_EXTI_GET_IT(GP(4)) != RESET)
	{
	    __HAL_GPIO_EXTI_CLEAR_IT(GP(4));
	    exti_call_callbacks(EXTI_4);
	}
}

void EXTI9_5_IRQHandler(void)
{
 	if(__HAL_GPIO_EXTI_GET_IT(GP(5)) != RESET)
	{
	    __HAL_GPIO_EXTI_CLEAR_IT(GP(5));
	    exti_call_callbacks(EXTI_5);
	}

	if(__HAL_GPIO_EXTI_GET_IT(GP(6)) != RESET)
	{
	    __HAL_GPIO_EXTI_CLEAR_IT(GP(6));
	    exti_call_callbacks(EXTI_6);
	}

	if(__HAL_GPIO_EXTI_GET_IT(GP(7)) != RESET)
	{
	    __HAL_GPIO_EXTI_CLEAR_IT(GP(7));
	    exti_call_callbacks(EXTI_7);
	}

	if(__HAL_GPIO_EXTI_GET_IT(GP(8)) != RESET)
	{
	    __HAL_GPIO_EXTI_CLEAR_IT(GP(8));
	    exti_call_callbacks(EXTI_8);
	}

	if(__HAL_GPIO_EXTI_GET_IT(GP(9)) != RESET)
	{
	    __HAL_GPIO_EXTI_CLEAR_IT(GP(9));
	    exti_call_callbacks(EXTI_9);
	}
}

void EXTI10_15_IRQHandler(void)
{
	if(__HAL_GPIO_EXTI_GET_IT(GP(10)) != RESET)
	{
	    __HAL_GPIO_EXTI_CLEAR_IT(GP(10));
	    exti_call_callbacks(EXTI_10);
	}

	if(__HAL_GPIO_EXTI_GET_IT(GP(11)) != RESET)
	{
	    __HAL_GPIO_EXTI_CLEAR_IT(GP(11));
	    exti_call_callbacks(EXTI_11);
	}

	if(__HAL_GPIO_EXTI_GET_IT(GP(12)) != RESET)
	{
	    __HAL_GPIO_EXTI_CLEAR_IT(GP(12));
	    exti_call_callbacks(EXTI_12);
	}

	if(__HAL_GPIO_EXTI_GET_IT(GP(13)) != RESET)
	{
	    __HAL_GPIO_EXTI_CLEAR_IT(GP(13));
	    exti_call_callbacks(EXTI_13);
	}

	if(__HAL_GPIO_EXTI_GET_IT(GP(14)) != RESET)
	{
	    __HAL_GPIO_EXTI_CLEAR_IT(GP(14));
	    exti_call_callbacks(EXTI_14);
	}

	if(__HAL_GPIO_EXTI_GET_IT(GP(15)) != RESET)
	{
	    __HAL_GPIO_EXTI_CLEAR_IT(GP(15));
	    exti_call_callbacks(EXTI_15);
	}
}








