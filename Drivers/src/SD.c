#include "SD.h"
#include <stdio.h>
#include "sdio.h"
#include "stm32f7xx_hal.h"

FATFS fs;
extern SD_HandleTypeDef sdmmc_handle;
 DSTATUS sd_status = STA_NOINIT;

FRESULT SD_init()
{
	uint8_t i;
	FRESULT ret = FR_OK;

	sdio_init();
	for(i = 0; i < 5; ++i)
	{
		disk_initialize(0);
		ret = f_mount(&fs, "", 1);
		if(ret == FR_OK)
			return FR_OK;
	}
	return ret;
}


DSTATUS disk_status (BYTE drv)
{
	HAL_SD_CardStateTypeDef status = (HAL_SD_GetCardState(&sdmmc_handle) == HAL_SD_CARD_TRANSFER ) ? 1 : 2;
	if(status == 1)
	{
	    sd_status &= ~STA_NOINIT;
	}

	return sd_status;
}

DSTATUS disk_initialize (BYTE drv)
{
	return disk_status(drv);
}

DRESULT disk_read(BYTE drv,BYTE *buff,DWORD sector,UINT count)
{
	if(sdio_read(drv, (uint8_t*) buff, sector, count))
	{
		return RES_OK;
	}
	else return RES_ERROR;
}

DRESULT disk_write (BYTE drv,const BYTE *buff,DWORD sector,UINT count)
{
	if(sdio_write(drv, (uint8_t*) buff, sector, count))
	{
		return RES_OK;
	}
	else return RES_ERROR;
}

DRESULT disk_ioctl (BYTE drv,BYTE ctrl,void *buff)
{
	DRESULT res = RES_ERROR;
	HAL_SD_CardInfoTypeDef card_info;
	HAL_SD_GetCardInfo(&sdmmc_handle, &card_info);

	if (sd_status == STA_NOINIT)
		return RES_NOTRDY;

	switch (ctrl)
	{
	  case CTRL_SYNC :
	    res = RES_OK;
	    break;

	  case GET_SECTOR_COUNT :
		  *(DWORD*)buff = card_info.LogBlockNbr;
		  res = RES_OK;
		  break;

	  case GET_SECTOR_SIZE :
	      *(WORD*)buff = card_info.LogBlockSize;
	      res = RES_OK;
	      break;

	  case GET_BLOCK_SIZE :
	      *(DWORD*)buff = card_info.LogBlockSize;
	      res = RES_OK;
	      break;

	  default:
	      res = RES_PARERR;
	}

	return res;
}
