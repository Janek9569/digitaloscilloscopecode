/*
 * This file is subject to the terms of the GFX License. If a copy of
 * the license was not distributed with this file, you can obtain one at:
 *
 *              http://ugfx.org/license.html
 */

#ifndef _GDISP_LLD_BOARD_H
#define _GDISP_LLD_BOARD_H

#include "stm32f7xx.h"
#include "fmc.h"
#include "PWM.h"
#include "ltdc.h"

#define  LTDC_HSYNC            ((uint16_t)10)   /* Horizontal synchronization */
#define  LTDC_HBP              ((uint16_t)100)   /* Horizontal back porch      */
#define  LTDC_HFP              ((uint16_t)210)   /* Horizontal front porch     */
#define  LTDC_VSYNC            ((uint16_t)10)   /* Vertical synchronization   */
#define  LTDC_VBP              ((uint16_t)10)    /* Vertical back porch        */
#define  LTDC_VFP              ((uint16_t)18)    /* Vertical front porch       */
#define  LTDC_WIDTH		1024
#define  LTDC_HEIGHT	600

GDisplay* g_display;

static const ltdcConfig driverCfg = {
	1024, 600,								// Width, Height (pixels)
	10, 10,									// Horizontal, Vertical sync (pixels)
	100, 10,								// Horizontal, Vertical back porch (pixels)
	210, 18,								// Horizontal, Vertical front porch (pixels)
	0,										// Sync flags
	0x000000,								// Clear color (RGB888)

	{										// Background layer config
		(LLDCOLOR_TYPE *)FMC_START_ADDR,	// Frame buffer address
		1024, 600,							// Width, Height (pixels)
		1024 * LTDC_PIXELBYTES,				// Line pitch (bytes)
		LTDC_PIXELFORMAT,					// Pixel format
		0, 0,								// Start pixel position (x, y)
		1024, 600,							// Size of virtual layer (cx, cy)
		LTDC_COLOR_FUCHSIA,					// Default color (ARGB8888)
		0x980088,							// Color key (RGB888)
		LTDC_BLEND_FIX1_FIX2,				// Blending factors
		0,									// Palette (RGB888, can be NULL)
		0,									// Palette length
		0xFF,								// Constant alpha factor
		LTDC_LEF_ENABLE						// Layer configuration flags
	},

	LTDC_UNUSED_LAYER_CONFIG				// Foreground layer config
};

static GFXINLINE void init_board(GDisplay* g) {

	// As we are not using multiple displays we set g->board to NULL as we don't use it.
	g->board = 0;
	g_display = g;
}

static GFXINLINE void post_init_board(GDisplay* g)
{
	ltdc_enable();
	(void)g;
}

static GFXINLINE void set_backlight(GDisplay* g, uint8_t percent)
{
	(void)g;
	PWM_set_val(PWM_LCD_ADJ, 60);
}

#endif /* _GDISP_LLD_BOARD_H */
