#ifndef INC_SCENE_CONTROL_H_
#define INC_SCENE_CONTROL_H_

#include <stdint.h>
#include "gfx.h"

typedef struct _Scene_control
{
	int16_t x;
	int16_t y;
	int16_t width;
	int16_t height;
	GDisplay* disp;
	GHandle label_name_handle;
	GHandle label_val_handler;
}Scene_control;

void scene_control_redraw(Scene_control* ctrl);
void scene_control_init(Scene_control* ctrl, int16_t x, int16_t y, int16_t width, int16_t height, GDisplay* disp);
void scene_control_set_name(Scene_control* ctrl, const char* nam, uint8_t redrawe);
void scene_control_set_value(Scene_control* ctrl, const char* val, uint8_t redraw);
void scene_control_enable_border(Scene_control* ctrl);
void scene_control_change_background_color(Scene_control* ctrl, uint32_t color);

#endif
