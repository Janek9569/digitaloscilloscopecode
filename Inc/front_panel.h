#ifndef BOOT_INC_FRONT_PANEL_H_
#define BOOT_INC_FRONT_PANEL_H_

#include <stdint.h>

#define ENCODER_MIDDLE_VALUE 2000
#define ENCODER_MAX_VALUE 4000
#define VALUE_PLUS 2
#define VALUE_MINUS 1

typedef enum _Encoder_type
{
	ENCODER_Y_A,//off
	ENCODER_Y_B,//off
	ENCODER_X_A,//att
	ENCODER_X_B,//att
	ENCODER_TIME,
	ENCODER_LAST
}Encoder_type;

typedef enum _Button_type
{
	BUTTON_1,
	BUTTON_2,
	BUTTON_3,
	BUTTON_4,
	BUTTON_A,
	BUTTON_B,
	BUTTON_LAST
}Button_type;

typedef enum _Registered_devices
{
	BUTTON_CH1,
	BUTTON_CH2,
	BUTTON_SINGLE,
	BUTTON_CURSOR,
	BUTTON_RUN_STOP,
	BUTTON_MENU,//other order than in panel
	ENCODER_CH1_POS,
	ENCODER_CH2_POS,
	ENCODER_CH1_ATT,
	ENCODER_CH2_ATT,
	ENCODER_TRIGGER,
	REGISTERED_DEVICE_LAST
}Registered_devices;

typedef struct __attribute__((packed)) _Command_pack
{
	uint8_t device;
	uint8_t val;
}Command_pack ;

void encoder_increment(Encoder_type encoder);
void encoder_derement(Encoder_type encoder);
void front_panel_init();
void front_panel_deinit();

int32_t front_panel_encoder_get_value(Encoder_type which);
void front_panel_set_callback(void(*f)(void), Registered_devices dev);
uint8_t is_encoder(Registered_devices device);
void front_panel_set_encoder_offset(Encoder_type which, int32_t off);

void front_panel_message_handler(void* arg);



#endif
