#ifndef _INC_LMH6881SQ_HPP_
#define _INC_LMH6881SQ_HPP_

#include "spi.h"
#include "global_utils.h"
#include "string.h"


#define SHUTDOWN_VALUE 0x00
#define TURN_ON_VALUE 0xFF
#define MAX_ATT_VALUE_HEX 80
#define WRITE_VAL 0
#define READ_VAL 1
#define DATA_PACKET_SIZE 2
#define MAX_ATT_dB 26

typedef enum _LMH_type
{
	LMH_A,
	LMH_B,
	LMH_LAST
}LMH_type;

void lmh_init(LMH_type type);
void lmh_deinit(LMH_type type);
void lmh_diasble(LMH_type type);
uint8_t lmh_enable(LMH_type type);
float lmh_get_att_VV(LMH_type type);
float lmh_get_att_dB(LMH_type type);
uint8_t lmh_is_enabled(LMH_type type);


#endif
