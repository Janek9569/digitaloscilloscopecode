#ifndef INC_SCENE_H_
#define INC_SCENE_H_

#include <stdint.h>
#include "Scene_control.h"

#define GRAPH_POS_X 0
#define GRAPH_POS_Y 60
#define GRAPH_WIDTH 800
#define GRAPH_HEIGHT 480

typedef enum _Scene_controls
{
	CONTROL_MODE,
	CONTROL_STATUS,
	CONTROL_LIMIT,
	CONTROL_PGA,
	CONTROL_OFFA,
	CONTROL_OFFB,
	CONTROL_DIVA,
	CONTROL_DIVB,
	CONTROL_TIME,
	CONTROL_MENU,
	CONTROL_PROBE,
	CONTROL_UNIT,
	CONTROL_CURSOR_1,
	CONTROL_CURSOR_2,
	CONTROL_LAST
}Scene_controls;

typedef struct _Scene_control_info
{
	int16_t x;
	int16_t y;
	int16_t width;
	int16_t height;
	const char* name;
	const char* def_val;
}Scene_control_info;

typedef struct _Scene
{
	 int16_t width, height;
	 Scene_control controls[CONTROL_LAST];
}Scene;

void scene_init();
int16_t scene_get_width();
Scene_control* scene_get_control(Scene_controls type);
void scene_set_ctrl_val(Scene_controls type, const char* val);
void scene_set_ctrl_backround(Scene_controls type, uint32_t color);

#endif
