#ifndef INC_CIRCULAR_SHIFT_REGISTER_H_
#define INC_CIRCULAR_SHIFT_REGISTER_H_

#include <stdint.h>
#include "FreeRTOS.h"
#include "semphr.h"

typedef struct _Circular_shift_register
{
	uint32_t frame_size;

	uint8_t* head;
	uint8_t* tail;

	uint8_t* _last_mem;
	uint8_t* _first_mem;

	uint32_t size;
	uint32_t element_size;

	uint32_t iterator;
	SemaphoreHandle_t sem;
}Circular_shift_register;

void circular_shift_register_init_static(Circular_shift_register* q, void* first_mem, uint32_t size, uint32_t element_size, uint32_t frame_size);
void* circular_shift_register_push(Circular_shift_register*q, void* element_data);
void* circular_shift_register_advance_iterator(Circular_shift_register*q);
void circular_shift_register_set_iterator(Circular_shift_register*q, uint32_t val);

#endif
