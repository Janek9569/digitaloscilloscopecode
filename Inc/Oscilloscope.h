#ifndef INC_OSCILLOSCOPE_H_
#define INC_OSCILLOSCOPE_H_

#include <stdint.h>
#include "FreeRTOS.h"
#include "task.h"

typedef enum _Oscilloscope_state
{
	O_STATE_INIT,//
	O_STATE_WAITING_FOR_FPGA_INT,//
	O_STATE_SETTING_FPGA_INT,//
	O_STATE_SETTING_EXTERNAL_INT,//
	O_STATE_WAITING_FOR_EXTERNAL_INT,//
	O_STATE_INJECT_NEW_DATA_TO_GRAPH,//
	O_STATE_REFRESH_GRAPH,//
	O_STATE_SET_STOPPED,
	O_STATE_STOPPED,//
	O_STATE_SET_VOLTAGE_OWF,//
	O_STATE_VOLTAGE_OWF,//
	O_STATE_SETTING_VALUE_TRIGGER,//
	O_STATE_WAITING_FOR_INTERNAL_VOL_INT,//
	O_STATE_SETTING_RUNNING,//
	O_STATE_RUNNING,//
	O_STATE_LAST
}Oscilloscope_state;

typedef struct _Oscilloscope
{
	uint32_t* sram_data_frame;
	TaskHandle_t cursors_task;
	TaskHandle_t menu_task;
	float time;
	float set_a_scale;
	float set_b_scale;
	int32_t set_a_y_off;
	int32_t set_b_y_off;

	volatile Oscilloscope_state state_before_OWF;
	volatile Oscilloscope_state state_after_redraw;
	volatile Oscilloscope_state state;
}Oscilloscope;

void oscilloscope_run();

#endif
