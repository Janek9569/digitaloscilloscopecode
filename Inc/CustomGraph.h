#ifndef INC_CUSTOM_GRAPH_H_
#define INC_CUSTOM_GRAPH_H_

#include "circular_shift_register.h"
#include "gfx.h"

#define CIRC_BUF_REDUNDANT_ELEMENTS 10

typedef struct _CustomGraphObject {
	GWindowObject g;  // Base Class

	int16_t	xorigin, yorigin;
	int16_t	lastx, lasty;
	int16_t x,y;

	uint8_t is_grid;
    uint8_t is_new_set;
	uint8_t* is_set_enabled;

	uint8_t set_count;

	Circular_shift_register* points;
	int16_t* y_offset;
	uint32_t* set_color;
	float* set_scale;

	uint16_t grid_height, grid_width;

	uint32_t x_axis_color;
} CustomGraphObject;

GHandle _gwinCustomGraphCreate(GDisplay *g, CustomGraphObject *cg, const GWindowInit *pInit, uint8_t set_count);
#define gwinCustomGraphCreate(cg, pInit)			_gwinCustomGraphCreate(GDISP, cg, pInit)
void gwinCustomGraphSetOrigin(GHandle gh, int16_t x, int16_t y);
void gwinCustomGraphAddPoint(GHandle gh, int16_t val, uint8_t set);
void gwinCustomGraphRedraw(GHandle gh);
void gwinCustomGraphToggleSet(GHandle gh, uint8_t set);
void gwinCustomGraphSetXAxisColor(GHandle gh, uint32_t val);
void gwinCustomGraphSetYOffset(GHandle gh, uint8_t set, uint16_t val);
void gwinCustomGraphSetScale(GHandle gh, uint8_t set, float val);
void gwinCustomGraphSetGrid(GHandle gh, uint8_t is_grid);
void gwinCustomGraphSetGridHeight(GHandle gh, uint16_t height);
void gwinCustomGraphSetGridWidth(GHandle gh, uint16_t width);

#endif
