#ifndef INC_MAIN_GRAPH_H_
#define INC_MAIN_GRAPH_H_

#include "circular_shift_register.h"
#include "gfx.h"
#include "CustomGraph.h"

typedef enum _Set
{
	SET_A,
	SET_B,
	SET_LAST
}Set;

typedef struct _Main_graph
{
	uint32_t posx;
	uint32_t posy;
	uint32_t width;
	uint32_t height;

	uint8_t act_graph;
	CustomGraphObject graph;
	GHandle graph_handle;
}Main_graph;

void graph_init(GDisplay* frame_handler, uint32_t posx, uint32_t posy, uint32_t width, uint32_t height);
void graph_set_display(GDisplay* gd);
void graph_redraw();
void graph_add_point(Set set, int16_t val);
void graph_toggle_set(Set set, uint8_t should_redraw);
void graph_set_y_offset(Set set, int16_t val, uint8_t should_redraw);
void graph_set_scale(Set set, float val, uint8_t should_redraw);
void graph_set_middle_color(uint32_t color);



#endif







