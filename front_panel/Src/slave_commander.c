#include <circular_queue.h>
#include "stm32f1xx_hal.h"
#include "slave_commander.h"
#include "slave_spi.h"
#include "buttons.h"
#include "encoders.h"
#include "global_utils.h"

static Circular_queue queue;
static uint8_t static_queue_buffer[COMMAND_QUEUE_SIZE * sizeof(Command_pack)];

static void latch_slave_flag()
{
	HAL_GPIO_WritePin(GPIOB, GP(5), GPIO_PIN_SET);
	for(volatile uint64_t i = 0; i < 10UL; ++i)
		asm("NOP");
	HAL_GPIO_WritePin(GPIOB, GP(5), GPIO_PIN_RESET);
}

//PB5
void commander_init()
{
	circular_queue_init(&queue, &static_queue_buffer, COMMAND_QUEUE_SIZE, sizeof(Command_pack));
	__HAL_RCC_GPIOB_CLK_ENABLE();
	GPIO_InitTypeDef GPIO_InitStruct;
	HAL_GPIO_WritePin(GPIOB, GP(5), GPIO_PIN_RESET);

	GPIO_InitStruct.Pin = GPIO_PIN_5;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	buttons_init();
	encoders_init();
	spi_init();
}

void commander_send_inc(uint32_t id)
{
	Command_pack new_element = { .device = id, .val = VALUE_PLUS };
	circular_queue_push(&queue, (uint8_t*) &new_element);
}

void commander_send_dec(uint32_t id)
{
	Command_pack new_element = { .device = id, .val = VALUE_MINUS };
	circular_queue_push(&queue, (uint8_t*) &new_element);
}

void commander_start()
{
	static Command_pack data_buffer;
	while(1)
	{
		if(circular_queue_pop(&queue, (uint8_t*) &data_buffer))
		{
			HAL_Delay(5);
			latch_slave_flag();
			spi_send((uint8_t*) &data_buffer, sizeof(Command_pack));
		}
	}
}


