#include "buttons.h"
#include "stm32f1xx_hal.h"
#include "global_utils.h"
#include "slave_commander.h"
#include "cfg.h"

//PD0 PB12 PA11 PA9 PA3 PA1
void buttons_init()
{
	static uint8_t _was_initialised = 0;
	ASSERT(_was_initialised++ == 0);

	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = GP(0);
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(12);
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(11) | GP(9)| GP(3)| GP(1);
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);


	HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI0_IRQn);

	HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI1_IRQn);

	HAL_NVIC_SetPriority(EXTI3_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI3_IRQn);

	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

	HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
}

void CH1_butt_exti_handler()
{
	commander_send_inc(BUTTON_CH1);
}

void CH2_butt_exti_handler()
{
	commander_send_inc(BUTTON_CH2);
}

void single_butt_exti_handler()
{
	commander_send_inc(BUTTON_SINGLE);
}

void cursor_butt_exti_handler()
{
	commander_send_inc(BUTTON_CURSOR);
}

void menu_butt_exti_handler()
{
	commander_send_inc(BUTTON_MENU);
}

void run_st_butt_exti_handler()
{
	commander_send_inc(BUTTON_RUN_STOP);
}
