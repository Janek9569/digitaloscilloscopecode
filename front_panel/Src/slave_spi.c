#include "slave_spi.h"
#include "stm32f1xx_hal.h"
#include "global_utils.h"
#include "cfg.h"

static SPI_HandleTypeDef hspi1;

void spi_init()
{
#ifndef PROGRAMMABLE_SPI_SLAVE
	hspi1.Instance = SPI1;
	hspi1.Init.Mode = SPI_MODE_SLAVE;
	hspi1.Init.Direction = SPI_DIRECTION_2LINES;
	hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
	hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
	hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
	hspi1.Init.NSS = SPI_NSS_SOFT;
	hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
	hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
	hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspi1.Init.CRCPolynomial = 10;
	ASSERT(HAL_SPI_Init(&hspi1) == HAL_OK);
#else
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
//PA13 clk
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = GP(13);
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, GPIO_PIN_RESET);

	GPIO_InitStruct.Pin = GP(3);
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOB, GP(3), GPIO_PIN_RESET);

	HAL_NVIC_SetPriority(EXTI3_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI3_IRQn);
#endif
}

#ifdef PROGRAMMABLE_SPI_SLAVE
static inline void slave_spi_data_set()
{
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_SET);
}

static inline void slave_spi_data_reset()
{
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);
}


static uint8_t* _data_to_send = NULL;
static uint16_t _data_len = 0;
static uint8_t _act_bit = 0;
static uint16_t _act_byte = 0;
static volatile uint8_t spi_slave_ready = 1;

static inline void spi_slave_clear_registers()
{
	_data_to_send = NULL;
	_data_len = 0;
	_act_bit = 0;
	_act_byte = 0;
	spi_slave_ready = 1;
}

void slave_spi_tick()
{
	if(_data_to_send)
	{
		if(_data_to_send[_act_byte] & ( 1 << _act_bit))
		{
			slave_spi_data_set();
		}
		else
		{
			slave_spi_data_reset();
		}
		++_act_bit;
		if(_act_bit >= 8)
		{
			_act_bit = 0;
			++_act_byte;
			if(_act_byte >= _data_len)
			{
				spi_slave_clear_registers();
			}
		}
	}
}

void spi_send(uint8_t* data, uint16_t data_len)
{
	_data_to_send = data;
	_data_len = data_len;
	_act_bit = 0;
	_act_byte = 0;
	spi_slave_ready = 0;

	while(!spi_slave_ready) {}
}

#else
void spi_send(uint8_t* data, uint16_t data_len)
{
	HAL_SPI_Transmit(&hspi1, data, data_len, WAIT_4_EVER);
}
#endif
/****INTS****/
void SPI1_IRQHandler(void)
{
	HAL_SPI_IRQHandler(&hspi1);
}






