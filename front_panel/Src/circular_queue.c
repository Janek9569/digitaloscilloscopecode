#include <circular_queue.h>
#include <string.h>
/*
 * POP -> HEAD.......TAIL <- PUSH_BACK
 */

#define GET_BUMP_PTR(q, ptr)							\
do{														\
	(ptr) += ((q)->element_size);						\
	if((ptr) >= ((q)->_last_mem))						\
		(ptr) = ((q)->_first_mem);						\
}while(0)

static uint8_t circular_queue_is_overflow(Circular_queue* q, void* ptr)
{
	if(ptr == q->head)
		return 1;
	else
		return 0;
}

void circular_queue_init(Circular_queue* q, void* first_mem, uint32_t size, uint32_t element_size)
{
	q->head = first_mem;
	q->tail = first_mem;
	q->_first_mem = first_mem;
	q->_last_mem = first_mem + (size * element_size);
	q->size = size;
	q->active_elements = 0;
	q->element_size = element_size;
}

uint8_t circular_queue_push(Circular_queue* q, uint8_t* element_data)
{
	uint8_t* alloc_next = q->tail;
	GET_BUMP_PTR(q, alloc_next);

	if(circular_queue_is_overflow(q, alloc_next))
		return 0;

	memcpy(q->tail, element_data, q->element_size);
	q->tail = alloc_next;
	q->active_elements += 1;
	return 1;
}

uint8_t circular_queue_is_empty(Circular_queue* q)
{
	if(q->head == q->tail)
		return 1;
	else
		return 0;
}

uint8_t circular_queue_pop(Circular_queue* q, uint8_t* buffer)
{
	if(circular_queue_is_empty(q))
		return 0;

	uint8_t* garbage_next = q->head;
	GET_BUMP_PTR(q, q->head);
	memcpy(buffer, garbage_next, q->element_size);
	q->active_elements -= 1;
	return 1;
}












