#include "stm32f1xx_hal.h"
#include "buttons.h"
#include "global_utils.h"

void HardFault_Handler(void)
{
	ERR_FATAL("Hard_fault", 0, 0, 0);
}

void MemManage_Handler(void)
{
	ERR_FATAL("Mem_Manage_fault", 0, 0, 0);
}

void BusFault_Handler(void)
{
	ERR_FATAL("Bus_fault", 0, 0, 0);
}

void UsageFault_Handler(void)
{
	ERR_FATAL("Useage_fault", 0, 0, 0);
}


void SVC_Handler		(void){}
void DebugMon_Handler	(void){}
void PendSV_Handler		(void){}
void NMI_Handler		(void){}


