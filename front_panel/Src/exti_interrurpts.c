#include "stm32f1xx_hal.h"
#include "buttons.h"
#include "encoders.h"
#include "cfg.h"
#include "slave_spi.h"

void EXTI0_IRQHandler(void)
{
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}

void EXTI1_IRQHandler(void)
{
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}

void EXTI2_IRQHandler(void)
{
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
}

void EXTI3_IRQHandler(void)
{
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_3);
}

void EXTI4_IRQHandler(void)
{
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_4);
}

void EXTI9_5_IRQHandler(void)
{
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_5);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_6);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_7);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_8);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_9);
}

void EXTI15_10_IRQHandler(void)
{
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_10);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_11);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_12);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_14);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_15);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	switch(GPIO_Pin)
	{
	case GPIO_PIN_0:
		run_st_butt_exti_handler();
		break;
	case GPIO_PIN_1:
		menu_butt_exti_handler();
		break;
	case GPIO_PIN_2:
		default_encoder_handler(ENCODER_SIGNAL_CH2_ATT_M);
		break;
	case GPIO_PIN_3:
		cursor_butt_exti_handler();
		break;
	case GPIO_PIN_4:
		default_encoder_handler(ENCODER_SIGNAL_TRIGGER_P);
		break;
	case GPIO_PIN_5:
		default_encoder_handler(ENCODER_SIGNAL_TRIGGER_M);
		break;
	case GPIO_PIN_6:
		default_encoder_handler(ENCODER_SIGNAL_CH1_ATT_P);
		break;
	case GPIO_PIN_7:
		default_encoder_handler(ENCODER_SIGNAL_CH1_ATT_M);
		break;
	case GPIO_PIN_8:
		default_encoder_handler(ENCODER_SIGNAL_CH2_POS_M);
		break;
	case GPIO_PIN_9:
		single_butt_exti_handler();
		break;
	case GPIO_PIN_10:
		default_encoder_handler(ENCODER_SIGNAL_CH2_ATT_P);
		break;
	case GPIO_PIN_11:
		CH2_butt_exti_handler();
		break;
	case GPIO_PIN_12:
		CH1_butt_exti_handler();
		break;
	case GPIO_PIN_13:
#ifndef PROGRAMMABLE_SPI_SLAVE
		default_encoder_handler(ENCODER_SIGNAL_CH1_POS_M);
#else
		slave_spi_tick();
#endif
		break;
	case GPIO_PIN_14:
		default_encoder_handler(ENCODER_SIGNAL_CH1_POS_P);
		break;
	case GPIO_PIN_15:
		default_encoder_handler(ENCODER_SIGNAL_CH2_POS_P);
		break;
	default:
		break;
	}
}
