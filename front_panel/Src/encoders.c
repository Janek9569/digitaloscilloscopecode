#include "encoders.h"
#include "stm32f1xx_hal.h"
#include <stdlib.h>
#include "global_utils.h"
#include "stm32f1xx_hal_gpio.h"
#include <string.h>

#define TOGGLE_IGNORE(x) ((x) = ~(x))
#define SIG2PIN(x) (sig_2_pin[x])
#define ENCODER_COUNT (ENCODER_SIGNAL_LAST/2)
#define FIRST_ENCODER ENCODER_CH1_POS
#define DEVICE_TO_TABLE_POS(x) ( (x) - FIRST_ENCODER )
#define SIGNAL_TO_DEVICE(x) ( signal_2_device[x] )

static void (*const encoder_callback[ENCODER_SIGNAL_LAST]) (void) =
{
		ch1_pos_p_handler,
		ch1_pos_m_handler,
		ch2_pos_p_handler,
		ch2_pos_m_handler,
		ch1_att_p_handler,
		ch1_att_m_handler,
		ch2_att_p_handler,
		ch2_att_m_handler,
		trigger_p_handler,
		trigger_m_handler
};

static uint8_t _is_signal_blocked[ENCODER_SIGNAL_LAST];
static uint8_t _signal_enable_time[ENCODER_SIGNAL_LAST];

static void disable_signal(Encoder_signal sig)
{
	_is_signal_blocked[sig] = 1;
}

static void enable_signal(Encoder_signal sig)
{
	_is_signal_blocked[sig] = 0;
}

static uint8_t is_singnal_blocked(Encoder_signal sig)
{
	return _is_signal_blocked[sig];
}

void encoders_update()
{
	for(uint8_t i = 0; i < ENCODER_SIGNAL_LAST; ++i)
	{
		if(is_singnal_blocked(i))
		{
			++_signal_enable_time[i];
			if(_signal_enable_time[i] >= SIGNAL_ENABLE_TIME)
			{
				_signal_enable_time[i] = 0;
				enable_signal(i);
			}
		}
	}
}

//PB13 PB14 PB15 PA10 PA8 PA7 PA6  PA5 PA4 PA2
void encoders_init()
{
	static uint8_t was_initialised = 0;
	ASSERT(was_initialised++ == 0);

	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = GP(10) | GP(6) | GP(4);
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(14) | GP(15);
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(8) | GP(7) | GP(2) | GP(5);
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(13);
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI2_IRQn);

	HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI4_IRQn);

	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

	HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
}

void encoder_inc(uint32_t id)
{
	commander_send_inc(id);
}

void encoder_dec(uint32_t id)
{
	commander_send_dec(id);
}

#define HANDLE_SIGNAL(sig)			\
do									\
{									\
	if(is_singnal_blocked(sig))		\
	{								\
		enable_signal(sig);			\
		return;						\
	}								\
}while(0)

void ch1_pos_m_handler()
{
	/*HANDLE_SIGNAL(ENCODER_SIGNAL_CH1_POS_M);
	disable_signal(ENCODER_SIGNAL_CH1_POS_P);

	encoder_dec(ENCODER_CH1_POS);*/
}

void ch1_pos_p_handler()
{
	/*HANDLE_SIGNAL(ENCODER_SIGNAL_CH1_POS_P);
	disable_signal(ENCODER_SIGNAL_CH1_POS_M);*/

	if(HAL_GPIO_ReadPin(GPIOB, GP(13)) == GPIO_PIN_SET)
		encoder_dec(ENCODER_CH1_POS);
	else
		encoder_inc(ENCODER_CH1_POS);
}

void ch2_pos_m_handler()
{/*
	HANDLE_SIGNAL(ENCODER_SIGNAL_CH2_POS_M);
	disable_signal(ENCODER_SIGNAL_CH2_POS_P);

	encoder_dec(ENCODER_CH2_POS);*/
}

void ch2_pos_p_handler()
{/*
	HANDLE_SIGNAL(ENCODER_SIGNAL_CH2_POS_P);
	disable_signal(ENCODER_SIGNAL_CH2_POS_M);

	encoder_inc(ENCODER_CH2_POS);*/

	if(HAL_GPIO_ReadPin(GPIOA, GP(8)) == GPIO_PIN_SET)
		encoder_dec(ENCODER_CH2_POS);
	else
		encoder_inc(ENCODER_CH2_POS);
}

void ch2_att_m_handler()
{/*
	HANDLE_SIGNAL(ENCODER_SIGNAL_CH2_ATT_M);
	disable_signal(ENCODER_SIGNAL_CH2_ATT_P);

	encoder_dec(ENCODER_CH2_ATT);*/
}

void ch2_att_p_handler()
{/*
	HANDLE_SIGNAL(ENCODER_SIGNAL_CH2_ATT_P);
	disable_signal(ENCODER_SIGNAL_CH2_ATT_M);

	encoder_inc(ENCODER_CH2_ATT);*/

	if(HAL_GPIO_ReadPin(GPIOA, GP(2)) == GPIO_PIN_SET)
		encoder_dec(ENCODER_CH2_ATT);
	else
		encoder_inc(ENCODER_CH2_ATT);
}

void ch1_att_m_handler()
{/*
	HANDLE_SIGNAL(ENCODER_SIGNAL_CH1_ATT_M);
	disable_signal(ENCODER_SIGNAL_CH1_ATT_P);

	encoder_dec(ENCODER_CH1_ATT);*/
}

void ch1_att_p_handler()
{/*
	HANDLE_SIGNAL(ENCODER_SIGNAL_CH1_ATT_P);
	disable_signal(ENCODER_SIGNAL_CH1_ATT_M);

	encoder_inc(ENCODER_CH1_ATT);*/

	if(HAL_GPIO_ReadPin(GPIOA, GP(7)) == GPIO_PIN_SET)
		encoder_dec(ENCODER_CH1_ATT);
	else
		encoder_inc(ENCODER_CH1_ATT);
}


void trigger_m_handler()
{/*
	HANDLE_SIGNAL(ENCODER_SIGNAL_TRIGGER_M);
	disable_signal(ENCODER_SIGNAL_TRIGGER_P);
	trace[i++] = ENCODER_SIGNAL_TRIGGER_M;

	encoder_dec(ENCODER_TRIGGER);*/
}

void trigger_p_handler()
{/*
	HANDLE_SIGNAL(ENCODER_SIGNAL_TRIGGER_P);
	disable_signal(ENCODER_SIGNAL_TRIGGER_M);

	encoder_inc(ENCODER_TRIGGER);*/

	if(HAL_GPIO_ReadPin(GPIOA, GP(5)) == GPIO_PIN_SET)
	{
		encoder_dec(ENCODER_TRIGGER);
	}
	else
	{
		encoder_inc(ENCODER_TRIGGER);
	}
}

void default_encoder_handler(Encoder_signal sig)
{
	encoder_callback[sig]();
}























