#include "rcc.h"
#include "slave_commander.h"
#include "global_utils.h"

int main(void)
{
	HAL_Init();
	rcc_init();
	commander_init();
	commander_start();

	ERR_FATAL("Commander returned a value!", NULL, NULL, NULL);

}
