#ifndef ENCODERS_H_
#define ENCODERS_H_

#include <stdint.h>
#include "slave_commander.h"
#define SIGNAL_ENABLE_TIME 600

typedef enum _Encoder_signal
{
	ENCODER_SIGNAL_CH1_POS_P,
	ENCODER_SIGNAL_CH1_POS_M,
	ENCODER_SIGNAL_CH2_POS_P,
	ENCODER_SIGNAL_CH2_POS_M,
	ENCODER_SIGNAL_CH1_ATT_P,
	ENCODER_SIGNAL_CH1_ATT_M,
	ENCODER_SIGNAL_CH2_ATT_P,
	ENCODER_SIGNAL_CH2_ATT_M,
	ENCODER_SIGNAL_TRIGGER_P,
	ENCODER_SIGNAL_TRIGGER_M,
	ENCODER_SIGNAL_LAST
} Encoder_signal;

void encoders_init();
void encoder_inc(uint32_t id);
void encoder_dec(uint32_t id);
void encoders_update();

void ch1_pos_m_handler(); //13
void ch1_pos_p_handler(); //14
void ch2_pos_m_handler(); //8
void ch2_pos_p_handler(); //15
void ch2_att_m_handler(); //2
void ch2_att_p_handler(); //10
void ch1_att_m_handler(); //7
void ch1_att_p_handler(); //6
void trigger_m_handler(); //5
void trigger_p_handler(); //4

void default_encoder_handler(Encoder_signal device);


#endif
