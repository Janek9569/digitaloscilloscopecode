#ifndef BUTTONS_H_
#define BUTTONS_H_

void buttons_init();

void CH1_butt_exti_handler();
void CH2_butt_exti_handler();
void single_butt_exti_handler();
void cursor_butt_exti_handler();
void menu_butt_exti_handler();
void run_st_butt_exti_handler();

#endif
