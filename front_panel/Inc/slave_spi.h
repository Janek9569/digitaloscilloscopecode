#ifndef SLAVE_SPI_H_
#define SLAVE_SPI_H_
#include <stdint.h>
#include "cfg.h"

void spi_init();
void spi_send(uint8_t* data, uint16_t data_len);

#ifdef PROGRAMMABLE_SPI_SLAVE

void slave_spi_tick();


#endif

#endif
