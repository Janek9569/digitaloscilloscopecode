#ifndef SLAVE_COMMANDER_H_
#define SLAVE_COMMANDER_H_

#include <stdint.h>
#define COMMAND_QUEUE_SIZE 8000
#define VALUE_PLUS 1
#define VALUE_MINUS 2

typedef enum _Registered_devices
{
	BUTTON_CH1,
	BUTTON_CH2,
	BUTTON_SINGLE,
	BUTTON_CURSOR,
	BUTTON_MENU,
	BUTTON_RUN_STOP,
	ENCODER_CH1_POS,
	ENCODER_CH2_POS,
	ENCODER_CH1_ATT,
	ENCODER_CH2_ATT,
	ENCODER_TRIGGER,
	REGISTERED_DEVICE
}Registered_defices;


typedef struct __attribute__((packed)) _Command_pack
{
	uint8_t device;
	uint8_t val;
} Command_pack;

void commander_init();
void commander_start();
void commander_send_inc(uint32_t id);
void commander_send_dec(uint32_t id);

#endif
