#ifndef CIRCULAR_QUEUE_H_
#define CIRCULAR_QUEUE_H_

#include "slave_commander.h"
#include <stdint.h>


typedef struct _Circular_queue
{
	uint8_t* head;
	uint8_t* tail;

	uint8_t* _last_mem;
	uint8_t* _first_mem;

	uint32_t size;
	uint32_t active_elements;
	uint32_t element_size;

} Circular_queue;

void circular_queue_init(Circular_queue* q, void* first_mem, uint32_t size, uint32_t element_size);
uint8_t circular_queue_push(Circular_queue*q, uint8_t* element_data);
uint8_t circular_queue_pop(Circular_queue* q, uint8_t* buffer);
uint8_t circular_queue_is_empty(Circular_queue* q);

#endif
