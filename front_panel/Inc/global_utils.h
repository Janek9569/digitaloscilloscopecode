#ifndef SRC_GLOBAL_UTILS_HPP_
#define SRC_GLOBAL_UTILS_HPP_

#include <stdio.h>
#include <stdint.h>

#define GP(num) (GPIO_PIN_##num)

#define WAIT_4_EVER 0xFFFFFFFFUL
#define ERR_BUFFER_SIZE 150
extern char msg_buffer[ERR_BUFFER_SIZE];

#define ERR_FATAL(msg, val1, val2, val3)								\
		_Pragma("GCC diagnostic push")									\
		_Pragma("GCC diagnostic ignored \"-Wformat-extra-args\"")		\
do{																		\
	snprintf(msg_buffer, ERR_BUFFER_SIZE, msg, val1, val2, val3);		\
	error_handler();													\
		_Pragma("GCC diagnostic pop")									\
}while(0)

#define str(x) (#x)

#define ASSERT(x)																									\
do{																													\
	if(!(x))																										\
	{																												\
		snprintf(msg_buffer, ERR_BUFFER_SIZE, 																		\
		"ASSERTION [ %s ] in function %s, in line %d", str(x), __FUNCTION__, __LINE__ );							\
		error_handler();																							\
	}																												\
}while(0)

#define _USED(x) ( (void) (x) )

void error_handler();

#endif
