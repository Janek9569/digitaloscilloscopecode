module Fpga_communicator(
/*ADC_IO*/
	input[31:0] ADC_data,
	input adc_clk,
/*SRAM_A_IO*/
	inout wire[31:0] SRAM_data,
	output wire[19:0] addr,
	output wire WE_A,
	output wire OE_A,
	output wire CE_A,
/*SRAM_B_IO*/
	output wire WE_B,
	output wire OE_B,
	output wire CE_B,
/*F7_IO*/
	output reg[3:0] data,
	output wire OWF,
	input reset,
	input writing_enabled,
	input out_clk,
/*dbg*/
	output wire reset_toggler
);

localparam
	nibble1 = 4'b0000,
	nibble2 = 4'b0001,
	nibble3 = 4'b0010,
	nibble4 = 4'b0011,
	nibble5 = 4'b0100,
	nibble6 = 4'b0101,
	nibble7 = 4'b0111,
	nibble8 = 4'b1000;

reg[3:0] nibble_ind = 4'h0;
wire[31:0] F7_handler_data;
reg sram_clk;

SRAM_handler sram(
/*SRAM IO*/
	.ADC_data( ADC_data ),
	.adc_clk( adc_clk ),
	
	.clk( sram_clk ),
	.reset( reset ),
	
	.SRAM_data( SRAM_data ),
	.addr( addr ),
	.F7_handler_data( F7_handler_data ),
	.OWF( OWF ),
	.WE_A( WE_A ),
	.OE_A( OE_A ),
	.CE_A( CE_A ),
	.WE_B( WE_B ),
	.OE_B( OE_B ),
	.CE_B( CE_B ),
	.writing_enabled( writing_enabled )
);


always@ (posedge out_clk) begin
	if(reset) begin
		nibble_ind <= nibble1;
		data <= F7_handler_data[3:0];
		sram_clk <= ~sram_clk;
	end
	else begin
		case(nibble_ind)
			nibble1: begin
				data <= F7_handler_data[3:0];
				sram_clk <= 0;
				nibble_ind <= nibble2;
			end
			nibble2: begin
				data <= F7_handler_data[7:4];
				nibble_ind <= nibble3;
			end
			
			nibble3: begin
				data <= F7_handler_data[11:8];
				nibble_ind <= nibble4;
			end
			
			nibble4: begin
				data <= F7_handler_data[15:12];
				nibble_ind <= nibble5;
			end
			
			nibble5: begin
				data <= F7_handler_data[19:16];
				nibble_ind <= nibble6;
			end
			
			nibble6: begin
				data <= F7_handler_data[23:20];
				nibble_ind <= nibble7;
			end
			
			nibble7: begin
				data <= F7_handler_data[27:24];
				nibble_ind <= nibble8;
			end
			
			nibble8: begin
				data <= F7_handler_data[31:28];
				//this is last nibble so read next word from SRAM -> rise sram clock
				sram_clk <= 1;
				nibble_ind <= nibble1;
			end
		endcase
	end
end

endmodule
