module SRAM_handler (
/*SRAM IO*/
	input wire[31:0] ADC_data,
	input wire adc_clk,
	
	input wire clk,
	input wire reset,
	
	inout[31:0] SRAM_data,
	output wire[19:0] addr,
	output reg[31:0] F7_handler_data,
	input writing_enabled,
	output wire OWF,
	output wire WE_A,
	output wire OE_A,
	output wire CE_A,
	output wire WE_B,
	output wire OE_B,
	output wire CE_B
);
	
localparam 
	WE_ENABLED = 0,
	WE_DISABLED = 1;
	
localparam 
	OE_ENABLED = 0,
	OE_DISABLED = 1;
	
localparam 
	CE_ENABLED = 0,
	CE_DISABLED = 1;
	
reg WE = WE_DISABLED;
assign WE_A = WE;
assign WE_B = WE;

reg OE = OE_ENABLED;
assign OE_A = OE;
assign OE_B = OE;

reg CE = CE_ENABLED;
assign CE_A = CE;
assign CE_B = CE;

wire global_clk;
assign global_clk = ((WE == WE_ENABLED) ? adc_clk : 1'b0) | clk;

reg[20:0] SRAM_addr_reg;
assign {OWF, addr} = SRAM_addr_reg[20:0];

reg[31:0] data_out;
assign SRAM_data = (WE == WE_ENABLED) ? data_out : 32'bz;

wire[31:0] data_in;
assign data_in = (WE == WE_ENABLED) ? 32'bz : SRAM_data;

reg skip_data = 1'h0;

always@ (posedge global_clk) begin
	if(reset) begin// reset is on
		data_out <= 32'h0;
		SRAM_addr_reg <= 21'h0;
		F7_handler_data <= data_in;
		OE <= OE_ENABLED;
		CE <= CE_ENABLED;
		if(writing_enabled) begin
			WE <= WE_ENABLED;
		end
		else begin
			WE <= WE_DISABLED;
		end
	end
	else begin
		if(OWF == 0) begin
			if( WE == WE_ENABLED ) begin
				if(skip_data) begin
					skip_data <= 0;
				end
				else begin
					skip_data <= 1;
					SRAM_addr_reg <= SRAM_addr_reg + 1;
				end
				data_out <= SRAM_addr_reg;
			end
		
			else begin
				F7_handler_data <= data_in;
				SRAM_addr_reg <= SRAM_addr_reg + 1;
			end
		end
	end
end

endmodule


