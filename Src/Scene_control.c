#include "Scene_control.h"

#define S_C_COL RGB2COLOR(105, 56, 217)

static const GWidgetStyle scene_control_style =
{
	.background = S_C_COL,
	.focus = S_C_COL,
	.enabled = {Black, Black, S_C_COL, S_C_COL},
	.disabled = {Black, Black, S_C_COL, S_C_COL},
	.pressed = {Black, Black, S_C_COL, S_C_COL}
};

void scene_control_init(Scene_control* ctrl, int16_t x, int16_t y, int16_t width, int16_t height, GDisplay* disp)
{
	ctrl->height = height;
	ctrl->width = width;
	ctrl->x = x;
	ctrl->y = y;
	ctrl->disp = disp;

	{
		GWidgetInit		wi;
		wi.customDraw = 0;
		wi.customParam = 0;
		wi.customStyle = &scene_control_style;
		wi.g.show = TRUE;
		wi.g.y = y;
		wi.g.x = x;
		wi.g.width = width;
		wi.g.height = height/4;
		wi.text = " ";


		ctrl->label_name_handle = gwinLabelCreate(NULL, &wi);

		wi.g.y = y + height/4;
		wi.g.height = (height * 3)/4;
		ctrl->label_val_handler = gwinLabelCreate(NULL, &wi);
	}

}

void scene_control_redraw(Scene_control* ctrl)
{
	gwinLabelDrawJustifiedCenter((GWidgetObject*)ctrl->label_name_handle, NULL);
	gwinLabelDrawJustifiedCenter((GWidgetObject*)ctrl->label_val_handler, NULL);
}

void scene_control_enable_border(Scene_control* ctrl)
{
	gwinLabelSetBorder(ctrl->label_name_handle, 1);
	gwinLabelSetBorder(ctrl->label_val_handler, 1);
}

void scene_control_set_name(Scene_control* ctrl, const char* name, uint8_t redraw)
{
	gwinSetText(ctrl->label_name_handle, name, TRUE);
	if(redraw)
		scene_control_redraw(ctrl);
}

void scene_control_set_value(Scene_control* ctrl, const char* val, uint8_t redraw)
{
	gwinSetText(ctrl->label_val_handler, val, TRUE);
	if(redraw)
		scene_control_redraw(ctrl);
}

void scene_control_change_background_color(Scene_control* ctrl, uint32_t color)
{
	static GWidgetStyle new_style =
	{
		.background = S_C_COL,
		.focus = S_C_COL,
		.enabled = {Black, Black, S_C_COL, S_C_COL},
		.disabled = {Black, Black, S_C_COL, S_C_COL},
		.pressed = {Black, Black, S_C_COL, S_C_COL}
	};

	new_style.background = color;

	gwinSetStyle(ctrl->label_val_handler, &new_style);
	gwinSetStyle(ctrl->label_name_handle, &new_style);
}
