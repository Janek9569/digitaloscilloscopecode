#include "main_graph.h"
#include "math.h"
#include "global_utils.h"
#include "task.h"
#include <stdlib.h>

static Main_graph main_graph;

static void graph_deamon_handler(void* arg);

void graph_redraw()
{
	gdispGFillArea(	main_graph.graph_handle->display, main_graph.posx, main_graph.posy,
					main_graph.width, main_graph.height, Black);
	gwinCustomGraphRedraw(main_graph.graph_handle);
}

void graph_toggle_set(Set set, uint8_t should_redraw)
{
	ASSERT(set < SET_LAST);
	gwinCustomGraphToggleSet(main_graph.graph_handle, set);

	if(should_redraw)
		graph_redraw();
}

void graph_set_y_offset(Set set, int16_t val, uint8_t should_redraw)
{
	ASSERT(set < SET_LAST);
	gwinCustomGraphSetYOffset(main_graph.graph_handle, set, val);

	if(should_redraw)
		graph_redraw();
}

void graph_set_scale(Set set, float val, uint8_t should_redraw)
{
	ASSERT(set < SET_LAST);
	gwinCustomGraphSetScale(main_graph.graph_handle, set, val);

	if(should_redraw)
		graph_redraw();
}

void graph_set_middle_color(uint32_t color)
{
	gwinCustomGraphSetXAxisColor(main_graph.graph_handle, color);
}

void graph_add_point(Set set, int16_t val)
{
	gwinCustomGraphAddPoint(main_graph.graph_handle, val, set);
}

void graph_set_display(GDisplay* gd)
{
	main_graph.graph.g.display = gd;
}

void graph_init(GDisplay* frame_handler, uint32_t posx, uint32_t posy, uint32_t width, uint32_t height)
{
	main_graph.posx = posx;
	main_graph.posy = posy;
	main_graph.width = width;
	main_graph.height = height;

    {
        GWindowInit wi;

        wi.show = TRUE;
        wi.x = posx;
        wi.y = posy;
        wi.width = width;
        wi.height = height;
        main_graph.graph_handle = _gwinCustomGraphCreate(frame_handler, &main_graph.graph, &wi, SET_LAST);
    }

	//start with sine wave
	for(uint16_t i = 0; i < width; ++i)
	{
		int16_t val = 80*sin(2*0.2*M_PI*i/180);
		graph_add_point(SET_A, val);
		graph_add_point(SET_B, val);
	}

	gwinCustomGraphSetOrigin(main_graph.graph_handle, 0, height/2);
	gwinCustomGraphSetGrid(main_graph.graph_handle, true);
	gwinCustomGraphSetGridHeight(main_graph.graph_handle, 40);
	gwinCustomGraphSetGridHeight(main_graph.graph_handle, 40);

    //start graph deamon
	TaskHandle_t xHandle = NULL;
	ASSERT(xTaskCreate(graph_deamon_handler, "GRAPH", 256, (void*)0, 0, &xHandle) == pdPASS);
}

/***** TASKS *****/
void graph_deamon_handler(void* arg)
{
	while(1)
	{
	    vTaskDelay(100);
	}
}
