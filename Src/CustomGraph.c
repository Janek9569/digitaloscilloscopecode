#include "CustomGraph.h"
#include "gwin/gwin_class.h"
#include "gwin/gwin.h"
#include "gos/gos_freertos.h"
#include "global_utils.h"
#include <stdlib.h>

static const gwinVMT customGraphVMT = {
		"CustGraph",			// The classname
		sizeof(CustomGraphObject),	// The object size
		0,						// The destroy routine
		0,						// The redraw routine
		0,						// The after-clear routine
};

static const GGraphStyle GGraphDefaultStyle = {
	{ GGRAPH_POINT_DOT, 0, White },			// point
	{ GGRAPH_LINE_DOT, 2, Gray },			// line
	{ GGRAPH_LINE_SOLID, 0, White },		// x axis
	{ GGRAPH_LINE_SOLID, 0, White },		// y axis
	{ GGRAPH_LINE_NONE, 0, White, 0 },		// x grid
	{ GGRAPH_LINE_NONE, 0, White, 0 },		// y grid
	GWIN_GRAPH_STYLE_XAXIS_ARROWS|GWIN_GRAPH_STYLE_YAXIS_ARROWS		// flags
};

static void custom_graph_draw_point(CustomGraphObject *cg, int16_t x, int16_t y, uint32_t color)
{
	// Convert to device space. Note the y-axis is inverted.
	x += cg->g.x + cg->xorigin;
	y = cg->g.y + cg->g.height - 1 - cg->yorigin - y;

	gdispGDrawPixel(cg->g.display, x, y, color);
}

static void custom_graph_draw_line(CustomGraphObject *cg, int16_t x0, int16_t y0, int16_t x1, int16_t y1)
{
	// Convert to device space. Note the y-axis is inverted.
	x0 += cg->g.x + cg->xorigin;
	y0 = cg->g.y + cg->g.height - 1 - cg->yorigin - y0;
	x1 += cg->g.x + cg->xorigin;
	y1 = cg->g.y + cg->g.height - 1 - cg->yorigin - y1;

	gdispGDrawLine(cg->g.display, x0, y0, x1, y1, Blue);
}

static void _custom_graph_draw_line_dots(CustomGraphObject *cg, int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint32_t color)
{
	// Convert to device space. Note the y-axis is inverted.
	x0 += cg->g.x;
	y0 = cg->g.y + cg->g.height - 1 - y0;
	x1 += cg->g.x;
	y1 = cg->g.y + cg->g.height - 1 - y1;

	int16_t run_on = 1;
	int16_t run_off = -5;//dot width
	int16_t run = 0;
	int16_t	dy, dx;
	int16_t addx, addy;
	int16_t P, diff, i;
	// Use Bresenham's algorithm modified to draw a stylized line
		run = 0;
		if (x1 >= x0) {
			dx = x1 - x0;
			addx = 1;
		} else {
			dx = x0 - x1;
			addx = -1;
		}
		if (y1 >= y0) {
			dy = y1 - y0;
			addy = 1;
		} else {
			dy = y0 - y1;
			addy = -1;
		}

		if (dx >= dy) {
			dy *= 2;
			P = dy - dx;
			diff = P - dx;

			for(i=0; i<=dx; ++i) {
				if (run++ >= 0) {
					if (run >= run_on)
						run = run_off;
					gdispGDrawPixel(cg->g.display, x0, y0, color);
				}
				if (P < 0) {
					P  += dy;
					x0 += addx;
				} else {
					P  += diff;
					x0 += addx;
					y0 += addy;
				}
			}
		} else {
			dx *= 2;
			P = dx - dy;
			diff = P - dy;

			for(i=0; i<=dy; ++i) {
				if (run++ >= 0) {
					if (run >= run_on)
						run = run_off;
					gdispGDrawPixel(cg->g.display, x0, y0, color);
				}
				if (P < 0) {
					P  += dx;
					y0 += addy;
				} else {
					P  += diff;
					x0 += addx;
					y0 += addy;
				}
			}
		}
}

static void custom_graph_print_grid(CustomGraphObject *cg)
{
	uint16_t width_count = cg->g.width / cg->grid_width;
	uint16_t height_count = cg->g.height / cg->grid_height;

	//vertical lines
	for(uint16_t wc = 0; wc < width_count; ++wc)
	{
		_custom_graph_draw_line_dots(cg, wc * cg->grid_width, 0, wc * cg->grid_width, cg->g.height, SkyBlue);
	}

	//horizontal lines
	for(uint16_t hc = 0; hc < height_count; ++hc)
	{
		_custom_graph_draw_line_dots(cg, 0, hc * cg->grid_height, cg->g.width, hc * cg->grid_height, SkyBlue);
	}
}

GHandle _gwinCustomGraphCreate(GDisplay *g, CustomGraphObject *cg, const GWindowInit *pInit, uint8_t set_count)
{
	if (!(cg = (CustomGraphObject *)_gwindowCreate(g, &cg->g, pInit, &customGraphVMT, 0)))
		return 0;

	ASSERT(set_count > 0);

	cg->xorigin = cg->yorigin = 0;
	cg->lastx = cg->lasty = 0;
	cg->set_count = set_count;

	//prepare shift registers
	cg->points = gfxAlloc(sizeof(Circular_shift_register) * set_count);
	cg->is_set_enabled = gfxAlloc(sizeof(uint8_t) * set_count);
	cg->y_offset = gfxAlloc(sizeof(int16_t) * set_count);
	cg->set_scale = gfxAlloc(sizeof(float) * set_count);
	cg->set_color = gfxAlloc(sizeof(uint32_t) * set_count);
	cg->x_axis_color = Yellow;
	cg->grid_height = 40;
	cg->grid_width = 40;
	for(uint8_t i = 0; i < set_count; ++i)
	{
		cg->set_color[i] = Blue;
		cg->is_set_enabled[i] = 0xFF;
		cg->y_offset[i] = 0;
		cg->set_scale[i] = 1.0;
		int16_t* main_graph_circular_buffer = gfxAlloc( (pInit->width + CIRC_BUF_REDUNDANT_ELEMENTS) * sizeof(uint16_t));
		circular_shift_register_init_static(&cg->points[i], main_graph_circular_buffer, pInit->width + CIRC_BUF_REDUNDANT_ELEMENTS, sizeof(int16_t), pInit->width);
	}
    cg->is_new_set = 0;

	gwinGraphSetStyle((GHandle)cg, &GGraphDefaultStyle);
	gwinSetVisible((GHandle)cg, pInit->show);
	_gwinFlushRedraws(REDRAW_WAIT);
	return (GHandle)cg;
}

void gwinCustomGraphSetOrigin(GHandle gh, int16_t x, int16_t y)
{
	CustomGraphObject* cg = (CustomGraphObject*)gh;

	if (gh->vmt != &customGraphVMT)
		return;

	cg->xorigin = x;
	cg->yorigin = y;
}


void gwinCustomGraphAddPoint(GHandle gh, int16_t val, uint8_t set)
{
	CustomGraphObject* cg = (CustomGraphObject*)gh;

	ASSERT(set < cg->set_count);

	circular_shift_register_push(&(cg->points[set]), &val);
}

void gwinCustomGraphToggleSet(GHandle gh, uint8_t set)
{
	CustomGraphObject* cg = (CustomGraphObject*)gh;
	ASSERT(set < cg->set_count);

	cg->is_set_enabled[set] = ~cg->is_set_enabled[set];
}

void gwinCustomGraphSetGrid(GHandle gh, uint8_t is_grid)
{
	CustomGraphObject* cg = (CustomGraphObject*)gh;

	cg->is_grid = is_grid;
}

void gwinCustomGraphSetGridHeight(GHandle gh, uint16_t height)
{
	ASSERT(height > 0);
	CustomGraphObject* cg = (CustomGraphObject*)gh;
	cg->grid_height = height;
}

void gwinCustomGraphSetGridWidth(GHandle gh, uint16_t width)
{
	ASSERT(width > 0);
	CustomGraphObject* cg = (CustomGraphObject*)gh;
	cg->grid_width = width;
}

void gwinCustomGraphSetXAxisColor(GHandle gh, uint32_t val)
{
	CustomGraphObject* cg = (CustomGraphObject*)gh;
	cg->x_axis_color = val;
}

void gwinCustomGraphSetYOffset(GHandle gh, uint8_t set, uint16_t val)
{
	CustomGraphObject* cg = (CustomGraphObject*)gh;
	ASSERT(set < cg->set_count);

	cg->y_offset[set] = val;
}

void gwinCustomGraphSetScale(GHandle gh, uint8_t set, float val)
{
	CustomGraphObject* cg = (CustomGraphObject*)gh;
	ASSERT(set < cg->set_count);

	cg->set_scale[set] = val;
}

void gwinCustomGraphRedraw(GHandle gh)
{
	CustomGraphObject* cg = (CustomGraphObject*)gh;

	if (gh->vmt != &customGraphVMT || !_gwinDrawStart(gh))
		return;

	//print grid
	if(cg->is_grid)
		custom_graph_print_grid(cg);

	for(uint8_t i = 0; i < cg->set_count; ++i)
	{
		if(!cg->is_set_enabled[i])
			continue;

		int16_t y_off = cg->y_offset[i];
		float scale = cg->set_scale[i];

		circular_shift_register_set_iterator(&(cg->points[i]), 0);
		uint32_t act_x = 0;

		void* _y;
		while((_y = circular_shift_register_advance_iterator(&(cg->points[i]))) != NULL)
		{
			if(act_x >= cg->points[i].frame_size)
				break;

			custom_graph_draw_point(cg, act_x, cg->y_offset[i], cg->x_axis_color);

			int16_t y = (*((int16_t*)_y) * scale) + y_off;

			if(act_x)
				custom_graph_draw_line(cg, cg->lastx, cg->lasty, act_x, y);

			// Redraw the previous point because the line may have overwritten it
			if (act_x == 0)
				custom_graph_draw_point(cg, cg->lastx, cg->lasty, cg->set_color[i]);

			cg->lastx = act_x;
			cg->lasty = y;
			++act_x;
		}

		circular_shift_register_set_iterator(&(cg->points[i]), 0);
		act_x = 0;
		while((_y = circular_shift_register_advance_iterator(&(cg->points[i]))) != NULL)
		{
			if(act_x >= cg->points[i].frame_size)
				break;

			int16_t y = (*((int16_t*)_y) * scale) + y_off;

			custom_graph_draw_point(cg, act_x, y, cg->set_color[i]);
			++act_x;
		}
	}

	_gwinDrawEnd(gh);
}



