#include "Oscilloscope.h"
#include "global_utils.h"
#include "FreeRTOS.h"
#include "task.h"
#include "exti.h"
#include "scene.h"
#include "front_panel.h"
#include "adc.h"
#include "fpga_communicator.h"
#include "Scene_control.h"
#include "LMH6881SQ.h"
#include "gfx.h"
#include "main_graph.h"

#define MODE_STOPPED_CLR RGB2COLOR(171, 11, 11)
#define MODE_STOPPED_TXT "STOPPED"
#define MODE_OVERFLO_CLR RGB2COLOR(255, 0, 0)
#define MODE_OVERFLO_TXT "VOLTAGE OVERFLOW"
#define MODE_EXT_TRG_CLR RGB2COLOR(32, 96, 97)
#define MODE_EXT_TRG_TXT "EXTERNAL TRIGGER"
#define MODE_VOL_TRG_CLR RGB2COLOR(139, 150, 22)
#define MODE_VOL_TRG_TXT "VOLTAGE TRIGGER"
#define MODE_RUNNING_CLR RGB2COLOR(66, 164, 12)
#define MODE_RUNNING_TXT "RUNNING"

static Oscilloscope oscilloscope;

static void oscilloscope_deamon_handler(void* arg);
static void oscilloscope_cursors_handler(void* arg);
static void oscilloscope_menu_handler(void* arg);

void oscilloscope_run()
{
	oscilloscope.state = O_STATE_INIT;
	oscilloscope.state_before_OWF = O_STATE_INIT;

	TaskHandle_t xHandle = NULL;
	ASSERT(xTaskCreate(oscilloscope_deamon_handler, "OSCILLO", 256, (void*)0, 0, &xHandle) == pdPASS);
}

/***** CALLBACKS *****/

static void oscilloscope_EXT_trigger_callback()
{
	oscilloscope.state = O_STATE_SETTING_FPGA_INT;
	exti_delete_callback_ISR(EXTI_2, oscilloscope_EXT_trigger_callback);
}

static void oscilloscope_touch_callback()
{
	return;
}

static void oscilloscope_OFW_callback()
{
	oscilloscope.state = O_STATE_SET_VOLTAGE_OWF;
}

static void oscilloscipe_fpga_cplt()
{
	oscilloscope.state = O_STATE_INJECT_NEW_DATA_TO_GRAPH;
	fpga_set_read_cpl_callback(NULL);
}

static void oscilloscope_run_start_butt_callback()
{
	if(oscilloscope.state == O_STATE_VOLTAGE_OWF)
	{
		oscilloscope.state = oscilloscope.state_before_OWF;
		front_panel_set_callback(NULL, BUTTON_RUN_STOP);
		return;
	} else if(oscilloscope.state == O_STATE_STOPPED)
	{
		//TODO:fill
	}
}

#define _STATE_OFF 0x00
#define _STATE_ON 0xFF
static void oscilloscope_cursor_butt_callback()
{
	static uint8_t state = _STATE_OFF;

	if(state)
		vTaskSuspend(oscilloscope.cursors_task);
	else
		vTaskResume(oscilloscope.cursors_task);

	state = ~state;
}

static void oscilloscope_ch1_butt_callback()
{
	if(oscilloscope.state != O_STATE_REFRESH_GRAPH)
	{
		graph_toggle_set(SET_A, false);
		oscilloscope.state_after_redraw = oscilloscope.state;
		oscilloscope.state = O_STATE_REFRESH_GRAPH;
	}
}

static void oscilloscope_ch2_butt_callback()
{
	if(oscilloscope.state != O_STATE_REFRESH_GRAPH)
	{
		graph_toggle_set(SET_B, false);
		oscilloscope.state_after_redraw = oscilloscope.state;
		oscilloscope.state = O_STATE_REFRESH_GRAPH;
	}
}

static void oscilloscope_menu_butt_callback()
{
	static uint8_t state = _STATE_OFF;

	if(state)
		vTaskSuspend(oscilloscope.menu_task);
	else
		vTaskResume(oscilloscope.menu_task);

	state = ~state;
}

static void oscilloscope_single_butt_callback()
{
	switch(oscilloscope.state_before_OWF)
	{/*
	case O_STATE_SETTING_EXTERNAL_INT:
		oscilloscope.state = O_STATE_SETTING_VALUE_TRIGGER;
		break;

	case O_STATE_SETTING_VALUE_TRIGGER:
		oscilloscope.state = O_STATE_SETTING_RUNNING;
		break;

	case O_STATE_SETTING_RUNNING:
		oscilloscope.state = O_STATE_SETTING_EXTERNAL_INT;
		break;
*/
	default:
		oscilloscope.state = O_STATE_SETTING_EXTERNAL_INT;
	}
}

void oscilloscope_ch1_off_callback()
{
	if(oscilloscope.state != O_STATE_REFRESH_GRAPH)
	{
		oscilloscope.state_after_redraw = oscilloscope.state;
		oscilloscope.state = O_STATE_REFRESH_GRAPH;
	}
}

void oscilloscope_ch2_off_callback()
{
	if(oscilloscope.state != O_STATE_REFRESH_GRAPH)
	{
		oscilloscope.state_after_redraw = oscilloscope.state;
		oscilloscope.state = O_STATE_REFRESH_GRAPH;
	}
}

void oscilloscope_ch1_att_callback()
{
	if(oscilloscope.state != O_STATE_REFRESH_GRAPH)
	{
		oscilloscope.state_after_redraw = oscilloscope.state;
		oscilloscope.state = O_STATE_REFRESH_GRAPH;
	}
}

void oscilloscope_ch2_att_callback()
{
	if(oscilloscope.state != O_STATE_REFRESH_GRAPH)
	{
		oscilloscope.state_after_redraw = oscilloscope.state;
		oscilloscope.state = O_STATE_REFRESH_GRAPH;
	}
}

void oscilloscope_trigger_callback()
{
	//TODO: fill
}

/****** TASKS ******/
static void oscilloscope_deamon_handler(void* arg)
{
	while(1)
	{
		switch(oscilloscope.state)
		{
		case O_STATE_SET_VOLTAGE_OWF:
			scene_set_ctrl_backround(CONTROL_MODE, MODE_OVERFLO_CLR);
			scene_set_ctrl_val(CONTROL_MODE, MODE_OVERFLO_TXT);

			scene_set_ctrl_val(CONTROL_STATUS, MODE_OVERFLO_TXT);

			adc_disable();
			lmh_diasble(LMH_A);
			lmh_diasble(LMH_B);


			oscilloscope.state = O_STATE_VOLTAGE_OWF;
			break;

		case O_STATE_VOLTAGE_OWF:
			vTaskDelay(5);
			break;

		case O_STATE_INIT:
			//EXT_TRIGGER PB2
			exti_init(EXTI_2, GP(2), GPIOB, GPIO_PULLDOWN, GPIO_SPEED_FREQ_VERY_HIGH, GPIO_MODE_IT_RISING);

			//T_PANEL PJ14
			//exti_init(EXTI_14, GP(14), GPIOJ, GPIO_PULLDOWN, GPIO_SPEED_FREQ_VERY_HIGH, EXTI_MODE_RISING);
			//exti_add_callback(EXTI_14, oscilloscope_touch_callback);

			//ADC_B_OWF PA0
			exti_init_prio(EXTI_0, ADC_OWFA_PIN, ADC_OWFA_PORT, GPIO_PULLDOWN, GPIO_SPEED_FREQ_VERY_HIGH, GPIO_MODE_IT_RISING, 0);
			exti_add_callback(EXTI_0, oscilloscope_OFW_callback);

			//ADC_A_OWF PH3
			exti_init_prio(EXTI_3, ADC_OWFB_PIN, ADC_OWFB_PORT, GPIO_PULLDOWN, GPIO_SPEED_FREQ_VERY_HIGH, GPIO_MODE_IT_RISING, 0);
			exti_add_callback(EXTI_3, oscilloscope_OFW_callback);

			//FPGA_INITIALIZATION
			oscilloscope.sram_data_frame = pvPortMalloc(sizeof(uint32_t) * scene_get_width());
			fpga_set_data_ptr(oscilloscope.sram_data_frame);
			fpga_set_read_cpl_callback( NULL );
			fpga_set_fulfill_alg(FULFILL_ALG_SCENE_WIDTH);

			fpga_set_READ();

			//ADC_INITIALIZATION
			DO_WHILE_SUCCESS(adc_enable());
			DO_WHILE_SUCCESS(lmh_enable(LMH_A));
			DO_WHILE_SUCCESS(lmh_enable(LMH_B));

			ASSERT(xTaskCreate(oscilloscope_cursors_handler, "OSC_CURS", 256, (void*)0, 0, &oscilloscope.cursors_task) == pdPASS);
			vTaskSuspend(oscilloscope.cursors_task);
			ASSERT(xTaskCreate(oscilloscope_menu_handler, "OSC_MENU", 256, (void*)0, 0, &oscilloscope.menu_task) == pdPASS);
			vTaskSuspend(oscilloscope.menu_task);

			//buttons
			front_panel_set_callback(oscilloscope_run_start_butt_callback, BUTTON_RUN_STOP);
			front_panel_set_callback(oscilloscope_cursor_butt_callback, BUTTON_CURSOR);
			front_panel_set_callback(oscilloscope_ch1_butt_callback, BUTTON_CH1);
			front_panel_set_callback(oscilloscope_ch2_butt_callback, BUTTON_CH2);
			front_panel_set_callback(oscilloscope_menu_butt_callback, BUTTON_MENU);
			front_panel_set_callback(oscilloscope_single_butt_callback, BUTTON_SINGLE);

			//encoders
			front_panel_set_callback(oscilloscope_ch1_off_callback, ENCODER_CH1_POS);
			front_panel_set_callback(oscilloscope_ch2_off_callback, ENCODER_CH2_POS);
			front_panel_set_callback(oscilloscope_ch1_att_callback, ENCODER_CH1_ATT);
			front_panel_set_callback(oscilloscope_ch2_att_callback, ENCODER_CH2_ATT);
			front_panel_set_callback(oscilloscope_trigger_callback, ENCODER_TRIGGER);

			//graph
			front_panel_set_encoder_offset(ENCODER_Y_A, 120);
			front_panel_set_encoder_offset(ENCODER_Y_B, -120);
			oscilloscope.set_a_y_off = front_panel_encoder_get_value(ENCODER_Y_A) - ENCODER_MIDDLE_VALUE;
			oscilloscope.set_b_y_off = front_panel_encoder_get_value(ENCODER_Y_B) - ENCODER_MIDDLE_VALUE;
			oscilloscope.set_a_scale = (front_panel_encoder_get_value(ENCODER_X_A) - ENCODER_MIDDLE_VALUE) * 0.01f + 1.0;
			oscilloscope.set_b_scale = (front_panel_encoder_get_value(ENCODER_X_B) - ENCODER_MIDDLE_VALUE) * 0.01f + 1.0;

			oscilloscope.state_before_OWF = O_STATE_LAST;
			oscilloscope.state_after_redraw = O_STATE_SETTING_EXTERNAL_INT;
			oscilloscope.state = O_STATE_REFRESH_GRAPH;
			break;

		case O_STATE_SETTING_EXTERNAL_INT:
			oscilloscope.state_before_OWF = O_STATE_SETTING_EXTERNAL_INT;
			scene_set_ctrl_backround(CONTROL_MODE, MODE_EXT_TRG_CLR);
			scene_set_ctrl_val(CONTROL_MODE, MODE_EXT_TRG_TXT);

			exti_add_callback(EXTI_2, oscilloscope_EXT_trigger_callback);
			oscilloscope.state = O_STATE_WAITING_FOR_EXTERNAL_INT;

			scene_set_ctrl_val(CONTROL_STATUS, "WAITINIG FOR EXTERNAL TRIGGER");
			break;

		case O_STATE_WAITING_FOR_EXTERNAL_INT:
			vTaskDelay(5);
			break;

		case O_STATE_SETTING_FPGA_INT:
			fpga_set_WRITE();
			fpga_set_read_cpl_callback( oscilloscipe_fpga_cplt );
			oscilloscope.state = O_STATE_WAITING_FOR_FPGA_INT;
			scene_set_ctrl_val(CONTROL_STATUS, "WAITINIG FOR DATA");
			break;

		case O_STATE_WAITING_FOR_FPGA_INT:
			vTaskDelay(5);
			break;

		case O_STATE_INJECT_NEW_DATA_TO_GRAPH:
		{
			int16_t a_smpl = 0, b_smpl = 0;

			for(int16_t i = 0; i < scene_get_width(); ++i)
			{
				//a_smpl = ; b_smpl = ;
				graph_add_point(SET_A, a_smpl);
				graph_add_point(SET_B, b_smpl);
			}

			oscilloscope.state_after_redraw = O_STATE_SET_STOPPED;
			oscilloscope.state = O_STATE_REFRESH_GRAPH;
			break;
		}

		case O_STATE_REFRESH_GRAPH:
			oscilloscope.set_a_y_off = front_panel_encoder_get_value(ENCODER_Y_A) - ENCODER_MIDDLE_VALUE;
			oscilloscope.set_b_y_off = front_panel_encoder_get_value(ENCODER_Y_B) - ENCODER_MIDDLE_VALUE;
			oscilloscope.set_a_scale = (front_panel_encoder_get_value(ENCODER_X_A) - ENCODER_MIDDLE_VALUE) * 0.01f + 1.0;
			oscilloscope.set_b_scale = (front_panel_encoder_get_value(ENCODER_X_B) - ENCODER_MIDDLE_VALUE) * 0.01f + 1.0;

			//update scale
			graph_set_scale(SET_A, oscilloscope.set_a_scale, false);
			graph_set_scale(SET_B, oscilloscope.set_b_scale, false);
			graph_set_y_offset(SET_A, oscilloscope.set_a_y_off, false);
			graph_set_y_offset(SET_B, oscilloscope.set_b_y_off, false);

			graph_redraw();

			if(oscilloscope.state_after_redraw != O_STATE_LAST)
				oscilloscope.state = oscilloscope.state_after_redraw;
			else
				oscilloscope.state = O_STATE_SET_STOPPED;

			break;

		case O_STATE_SET_STOPPED:
			scene_set_ctrl_backround(CONTROL_MODE, MODE_STOPPED_CLR);
			scene_set_ctrl_val(CONTROL_MODE, MODE_STOPPED_TXT);
			oscilloscope.state = O_STATE_STOPPED;
			scene_set_ctrl_val(CONTROL_STATUS, "STOPPED");
			break;

		case O_STATE_STOPPED:
		{
			vTaskDelay(5);
			break;
		}

		case O_STATE_SETTING_VALUE_TRIGGER:
		{
			oscilloscope.state_before_OWF = O_STATE_SETTING_VALUE_TRIGGER;
			scene_set_ctrl_backround(CONTROL_MODE, MODE_VOL_TRG_CLR);
			scene_set_ctrl_val(CONTROL_MODE, MODE_VOL_TRG_TXT);
			oscilloscope.state = O_STATE_WAITING_FOR_INTERNAL_VOL_INT;
			scene_set_ctrl_val(CONTROL_STATUS, "WAITING FOR TRIGGER");
			//not implemented yet
			break;
		}

		case O_STATE_WAITING_FOR_INTERNAL_VOL_INT:
			vTaskDelay(5);
			break;

		case O_STATE_SETTING_RUNNING:
		{
			oscilloscope.state_before_OWF = O_STATE_SETTING_RUNNING;
			scene_set_ctrl_backround(CONTROL_MODE, MODE_RUNNING_CLR);
			scene_set_ctrl_val(CONTROL_MODE, MODE_RUNNING_TXT);
			oscilloscope.state = O_STATE_RUNNING;
			scene_set_ctrl_val(CONTROL_STATUS, "RUNNING");
			//not implemented yet
			break;
		}

		case O_STATE_RUNNING:
			vTaskDelay(5);
			//not implemented yet
			break;

		default:
			break;
		}
	}
}

static void oscilloscope_cursors_handler(void* arg)
{
	while(1)
	{

	}
}

static void oscilloscope_menu_handler(void* arg)
{
	while(1)
	{

	}
}





