#include "front_panel.h"
#include "spi.h"
#include "global_utils.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "exti.h"
#include <string.h>

static uint32_t encoder_value[ENCODER_LAST];
static Exti_type exti_type;
//static SPI_enumerator spi_en;
static void(*device_f[REGISTERED_DEVICE_LAST])(void);
static SemaphoreHandle_t front_panel_deamon_semaphr;

static void front_panel_int_handler()
{
	BaseType_t x;
	exti_delete_callback_ISR(exti_type, front_panel_int_handler);
	xSemaphoreGiveFromISR(front_panel_deamon_semaphr, &x);
}

static inline void front_panel_tick()
{
	HAL_GPIO_WritePin(GPIOF, GP(7), GPIO_PIN_SET);
	for(volatile uint64_t i = 0; i < 5000UL; ++i)
		asm("NOP");
	HAL_GPIO_WritePin(GPIOF, GP(7), GPIO_PIN_RESET);
}

static void	front_panel_recieve(uint8_t* data, uint32_t size)
{
	for(uint32_t byte = 0; byte < size; ++byte)
	{
		data[byte] = 0;
		for(uint8_t bit = 0; bit < 8; ++bit)
		{
			front_panel_tick();
			GPIO_PinState state = HAL_GPIO_ReadPin(GPIOH, GP(7));
			data[byte] |= (state << bit);
		}
	}
}

static void front_panel_actualise_status()
{
	Command_pack new_message;
	memset(&new_message, 0x00, sizeof(Command_pack));

	front_panel_recieve((uint8_t*)&new_message, sizeof(Command_pack));
	//spi_recieve(spi_en, (uint8_t*)&new_message, sizeof(Command_pack));

	exti_init(EXTI_6, GPIO_PIN_6, GPIOH, GPIO_PULLDOWN, GPIO_SPEED_FREQ_VERY_HIGH, GPIO_MODE_IT_RISING);
	exti_add_callback(EXTI_6, front_panel_int_handler);

	if(new_message.device < REGISTERED_DEVICE_LAST)
	{
		if(device_f[new_message.device])
			device_f[new_message.device]();

		if(is_encoder((Registered_devices)new_message.device))
		{
			if(new_message.val == VALUE_PLUS)
			{
				encoder_derement((Encoder_type)new_message.device - ENCODER_CH1_POS);
			}
			else if(new_message.val == VALUE_MINUS)
			{
				encoder_increment((Encoder_type)new_message.device - ENCODER_CH1_POS);
			}
		}
	}
}

void encoder_increment(Encoder_type encoder)
{
	if(encoder_value[encoder] < ENCODER_MAX_VALUE)
		++encoder_value[encoder];
}

void encoder_derement(Encoder_type encoder)
{
	if(encoder_value[encoder] > 0)
		--encoder_value[encoder];
}

void front_panel_init()
{
	for(uint32_t i = 0; i < ENCODER_LAST; ++i)
		encoder_value[i] = ENCODER_MIDDLE_VALUE;

	for(uint32_t i = 0; i < REGISTERED_DEVICE_LAST; ++i)
	{
		device_f[i] = NULL;
	}

	front_panel_deamon_semaphr = xSemaphoreCreateBinary();

	exti_type = EXTI_6;
	exti_init(EXTI_6, GP(6), GPIOH, GPIO_PULLDOWN, GPIO_SPEED_FREQ_VERY_HIGH, GPIO_MODE_IT_RISING);
	exti_add_callback(EXTI_6, front_panel_int_handler);

	TaskHandle_t xHandle = NULL;
	ASSERT(xTaskCreate(front_panel_message_handler, "FP_Deamon", 256, (void*)0, 0, &xHandle) == pdPASS);

	__HAL_RCC_GPIOH_CLK_ENABLE();

	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = GP(7);
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GP(7);
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);



	//spi_init(SPI5_EN);
	//spi_en = SPI5_EN;
}

void front_panel_deinit()
{
	exti_delete_callback(exti_type, front_panel_int_handler);
	vSemaphoreDelete(front_panel_deamon_semaphr);
}

int32_t front_panel_encoder_get_value(Encoder_type which)
{
	return encoder_value[which];
}


void front_panel_set_callback(void(*f)(void), Registered_devices dev)
{
	device_f[dev] = f;
}

uint8_t is_encoder(Registered_devices device)
{
	return ( (device >= ENCODER_CH1_POS) && (device < REGISTERED_DEVICE_LAST) );
}

void front_panel_set_encoder_offset(Encoder_type which, int32_t off)
{
	encoder_value[which] = ENCODER_MIDDLE_VALUE + off;
}

/***** RESOURCE DEAMONS *****/
void front_panel_message_handler(void* arg)
{
	for(;;)
	{
		xSemaphoreTake(front_panel_deamon_semaphr, WAIT_FOR_EVER);
		vTaskDelay(2);
		front_panel_actualise_status();
	}
}

