#include "circular_shift_register.h"
#include "global_utils.h"
#include "string.h"
/*
 * HEAD.......TAIL <- PUSH_BACK
 */

#define BUMP_PTR(q, ptr)								\
do{														\
	(ptr) += ((q)->element_size);						\
	if((ptr) >= ((q)->_last_mem))						\
		(ptr) = ((q)->_first_mem);						\
}while(0)

#define BUMP_ITERATOR(q)								\
do{														\
	(++((q)->iterator));								\
	if( ((q)->iterator) >= ((q)->frame_size) )			\
		((q)->iterator) = 0;							\
}while(0)

static void circular_shift_register_release_sem(Circular_shift_register* q)
{
	xSemaphoreGive(q->sem);
}

static void circular_shift_register_lock_sem(Circular_shift_register* q)
{
	xSemaphoreTake(q->sem, WAIT_FOR_EVER);
}

void circular_shift_register_init_static(Circular_shift_register* q, void* first_mem, uint32_t size, uint32_t element_size, uint32_t frame_size)
{
	q->frame_size = frame_size;
	q->head = first_mem;
	q->tail = first_mem + (frame_size * element_size);
	q->_first_mem = first_mem;
	q->_last_mem = first_mem + (size * element_size);
	q->size = size;
	q->element_size = element_size;

	q->sem = xSemaphoreCreateBinary();

	circular_shift_register_release_sem(q);
}

void* circular_shift_register_push(Circular_shift_register* q, void* element_data)
{
	circular_shift_register_lock_sem(q);
	uint8_t* garbage_next = q->head;

	uint8_t* alloc_next = q->tail;
	BUMP_PTR(q, alloc_next);

	memcpy(q->tail, element_data, q->element_size);
	q->tail = alloc_next;

	BUMP_PTR(q, q->head);
	circular_shift_register_release_sem(q);
	return garbage_next;
}

void* circular_shift_register_advance_iterator(Circular_shift_register*q)
{
	circular_shift_register_lock_sem(q);

	uint8_t* next_data_ptr;

	if(q->head > q->tail)
	{
		uint32_t head_tail_diff = (q->_last_mem - q->head) / q->element_size;
		if(q->iterator >= head_tail_diff)
			next_data_ptr = q->_first_mem + ((q->iterator - head_tail_diff)  * q->element_size);
		else
			next_data_ptr = q->head + ((q->iterator)  * q->element_size);
	}
	else
	{
		next_data_ptr = q->head + (q->iterator * q->element_size);
	}

	BUMP_ITERATOR(q);

	circular_shift_register_release_sem(q);

	if(q->iterator == 0)
		return NULL;

	return next_data_ptr;
}

void circular_shift_register_set_iterator(Circular_shift_register*q, uint32_t val)
{
	ASSERT(val < q->frame_size);
	q->iterator = val;
}







