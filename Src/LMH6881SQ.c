#include "LMH6881SQ.h"
#include "stm32f7xx_hal.h"
#include "math.h"
#include "exti.h"
#include "adc.h"

//#define LMH_PROGRAMMABLE_SPI
#undef LMH_PROGRAMMABLE_SPI

#define TEST_WRITING_VAL 79

typedef enum _LMH6881SQ_register_type
{
	LMH6881SQ_REVISION_ID,
	LMH6881SQ_PRODUCT_ID,
	LMH6881SQ_POWER_DOWN,
	LMH6881SQ_ATTENUATION,
	LMH6881SQ_REGISTER_TYPE_LAST
}LMH6881SQ_register_type;

typedef struct _LMH6881SQ_command
{
	unsigned int addr:4;
	unsigned int reserved:3;
	unsigned int RW:1;
}LMH6881SQ_command;

typedef struct _LMH_info
{
	SPI_enumerator spi_en;
	GPIO_TypeDef* gpio_port;
	uint16_t gpio_pin;
	uint8_t is_enabled;
	uint8_t register_value[LMH6881SQ_REGISTER_TYPE_LAST];
	uint8_t is_initialised;
}LMH_info;

static LMH_info registered_lmh[LMH_LAST] = //TODO: fill info
{
		{},
		{}
};

#ifdef LMH_PROGRAMMABLE_SPI
/*********************/
//MSB FIRST
static inline void _b_programmable_spi_init()
{

	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = GP(14);
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);//PB14 -> input


	HAL_GPIO_WritePin(GPIOB, GP(13), GPIO_PIN_RESET);
	GPIO_InitStruct.Pin = GP(13);
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;//PB13 -> clk
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_3, GPIO_PIN_RESET);
	GPIO_InitStruct.Pin = GPIO_PIN_3;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);//PC3 -> output

}

static inline void _spi_tick()
{
	HAL_GPIO_WritePin(GPIOB, GP(13), GPIO_PIN_SET);
	for(volatile uint64_t i = 0; i < 5000UL; ++i)
		asm("NOP");
	HAL_GPIO_WritePin(GPIOB, GP(13), GPIO_PIN_RESET);
}

static void _programmable_spi_send(uint8_t* data, uint8_t size)
{
	for(uint32_t byte = 0; byte < size; ++byte)
	{
		for(uint8_t bit = 0; bit < 8; ++bit)
		{
			GPIO_PinState state = (data[byte] & (1 << (7 - bit))) ? GPIO_PIN_SET : GPIO_PIN_RESET;
			HAL_GPIO_WritePin(GPIOC, GP(3), state);
			_spi_tick();
		}
	}
}

static void _programmable_spi_recieve(uint8_t* data, uint8_t size)
{
	for(uint32_t byte = 0; byte < size; ++byte)
	{
		data[byte] = 0;
		for(uint8_t bit = 0; bit < 8; ++bit)
		{
			_spi_tick();
			GPIO_PinState state = HAL_GPIO_ReadPin(GPIOB, GP(14));
			data[byte] |= (state <<  (7 - bit));
		}
	}
}
/*********************/
#endif

static LMH6881SQ_command __attribute__((used)) lmh_get_empty_command()
{
	LMH6881SQ_command new_cmd;
	memset(&new_cmd, 0x00, sizeof(LMH6881SQ_command));
	return new_cmd;
}

static LMH6881SQ_command lmh_get_command(uint8_t RW, uint8_t reg)
{
	LMH6881SQ_command new_cmd;
	new_cmd.RW = RW;
	new_cmd.addr = reg;
	new_cmd.reserved = 0;
	return new_cmd;
}

static uint8_t __attribute__((used)) lmh__read(LMH_type type, LMH6881SQ_register_type reg)
{
	LMH_info* act_info = &registered_lmh[type];
	(void)act_info;
	LMH6881SQ_command cmd = lmh_get_command(READ_VAL, reg);

#ifdef LMH_PROGRAMMABLE_SPI
	_programmable_spi_send((uint8_t*)&cmd, sizeof(LMH6881SQ_command));
	_programmable_spi_recieve((uint8_t*)&cmd, sizeof(LMH6881SQ_command));
#else

	spi__transmit(act_info->spi_en, (uint8_t*)&cmd, sizeof(LMH6881SQ_command));
	spi__recieve(act_info->spi_en, (uint8_t*)&cmd, sizeof(LMH6881SQ_command));
#endif
	return *((uint8_t*)&cmd);
}

static void __attribute__((used)) lmh__write(LMH_type type, LMH6881SQ_register_type reg, uint8_t val)
{
	LMH_info* act_info = &registered_lmh[type];
	(void)act_info;
	uint8_t data_packet[DATA_PACKET_SIZE];
	LMH6881SQ_command cmd = lmh_get_command(WRITE_VAL, reg);
	data_packet[0] = *((uint8_t*)&cmd);
	data_packet[1] = val;
#ifdef LMH_PROGRAMMABLE_SPI
	_programmable_spi_send((uint8_t*)&data_packet, DATA_PACKET_SIZE);
#else
	spi__transmit(act_info->spi_en, (uint8_t*)&data_packet, DATA_PACKET_SIZE);
#endif
}

static void __attribute__((used)) lmh_write(LMH_type type, LMH6881SQ_register_type reg, uint8_t val)
{
	LMH_info* act_info = &registered_lmh[type];
	ASSERT(reg != LMH6881SQ_REGISTER_TYPE_LAST);
	lmh__write(type, reg, val);
	act_info->register_value[reg] = val;
}

static uint8_t __attribute__((used)) lmh_read(LMH_type type, LMH6881SQ_register_type reg)
{
	LMH_info* act_info = &registered_lmh[type];
	ASSERT(reg != LMH6881SQ_REGISTER_TYPE_LAST);
	return act_info->register_value[reg];
}

static void __attribute__((used)) lmh__disable(LMH_type type)
{
	LMH_info* act_info = &registered_lmh[type];
	lmh__write(type, LMH6881SQ_POWER_DOWN, SHUTDOWN_VALUE);
	act_info->register_value[LMH6881SQ_POWER_DOWN] = SHUTDOWN_VALUE;
	HAL_GPIO_WritePin(act_info->gpio_port, act_info->gpio_pin, GPIO_PIN_SET);
}

static void PGA_OFW_callback();

void lmh_init(LMH_type type)
{
	ASSERT(type < LMH_LAST);
	LMH_info* act_info = &registered_lmh[type];
	ASSERT(act_info->is_initialised == false);
	act_info->is_initialised = true;

	GPIO_InitTypeDef GPIO_InitStruct;
	switch(type)
	{
	case LMH_A:
		//AIN_SD
		act_info->gpio_port = GPIOK;
		act_info->gpio_pin = GP(7);
		act_info->spi_en = SPI1_EN;
		__HAL_RCC_GPIOK_CLK_ENABLE();
		HAL_GPIO_WritePin(GPIOK, GP(7), GPIO_PIN_RESET);

		GPIO_InitStruct.Pin = GP(7);
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(GPIOK, &GPIO_InitStruct);

		spi_init(SPI1_EN);

		//ADC_A_OWF PH3
		exti_init_prio(EXTI_3, ADC_OWFB_PIN, ADC_OWFB_PORT, GPIO_PULLDOWN, GPIO_SPEED_FREQ_VERY_HIGH, GPIO_MODE_IT_RISING, 0);
		exti_add_callback(EXTI_3, PGA_OFW_callback);

		break;
	case LMH_B:
		//BIN_SD
		act_info->gpio_port = GPIOJ;
		act_info->gpio_pin = GP(0);
		act_info->spi_en = SPI2_EN;

		__HAL_RCC_GPIOJ_CLK_ENABLE();
		HAL_GPIO_WritePin(GPIOJ, GP(0), GPIO_PIN_RESET);

		GPIO_InitStruct.Pin = GP(0);
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(GPIOJ, &GPIO_InitStruct);

#ifdef LMH_PROGRAMMABLE_SPI
		_b_programmable_spi_init();
#else
		spi_init(SPI2_EN);
#endif

		//ADC_B_OWF PA0
		exti_init_prio(EXTI_0, ADC_OWFA_PIN, ADC_OWFA_PORT, GPIO_PULLDOWN, GPIO_SPEED_FREQ_VERY_HIGH, GPIO_MODE_IT_RISING, 0);
		exti_add_callback(EXTI_0, PGA_OFW_callback);
		break;

	default:
		ERR_FATAL("This LMH6881 is not supported in this device", NULL, NULL, NULL);
		break;
	}

	memset(act_info->register_value, 0x00, LMH6881SQ_REGISTER_TYPE_LAST);

	//send dummy clock
	lmh__write(type, LMH6881SQ_POWER_DOWN, SHUTDOWN_VALUE);

	//fulfill register value
	for(uint8_t i = 0; i < LMH6881SQ_REGISTER_TYPE_LAST; ++i)
		act_info->register_value[i] = lmh__read(type, (LMH6881SQ_register_type)i);

	lmh__disable(type);

	//check writing procedure
	lmh__write(type, LMH6881SQ_ATTENUATION, TEST_WRITING_VAL);
	ASSERT(lmh__read(type, LMH6881SQ_ATTENUATION) == TEST_WRITING_VAL);

	//set default value
	lmh__write(type, LMH6881SQ_ATTENUATION, MAX_ATT_VALUE_HEX);
	act_info->register_value[LMH6881SQ_ATTENUATION] = lmh__read(type, LMH6881SQ_ATTENUATION);
	ASSERT(act_info->register_value[LMH6881SQ_ATTENUATION] == TEST_WRITING_VAL);
}

void lmh_deinit(LMH_type type)
{
	ASSERT(type < LMH_LAST);
	LMH_info* act_info = &registered_lmh[type];
	act_info->is_initialised = 0;
	HAL_GPIO_DeInit(act_info->gpio_port, act_info->gpio_pin);
	spi_deinit(act_info->spi_en);
}

void lmh_diasble(LMH_type type)
{
	LMH_info* act_info = &registered_lmh[type];
	lmh_write(type, LMH6881SQ_POWER_DOWN, SHUTDOWN_VALUE);
	act_info->register_value[LMH6881SQ_POWER_DOWN] = SHUTDOWN_VALUE;
	HAL_GPIO_WritePin(act_info->gpio_port, act_info->gpio_pin, GPIO_PIN_SET);
}

static uint8_t lmh_is_OWF()
{
	if( (HAL_GPIO_ReadPin(ADC_OWFA_PORT, ADC_OWFA_PIN) == GPIO_PIN_SET) ||
		(HAL_GPIO_ReadPin(ADC_OWFB_PORT, ADC_OWFB_PIN) == GPIO_PIN_SET)    )
		return true;

	return false;
}

uint8_t lmh_enable(LMH_type type)
{
	if(!lmh_is_OWF())
	{
		LMH_info* act_info = &registered_lmh[type];
		lmh_write(type, LMH6881SQ_POWER_DOWN, TURN_ON_VALUE);
		act_info->register_value[LMH6881SQ_POWER_DOWN] = TURN_ON_VALUE;
		HAL_GPIO_WritePin(act_info->gpio_port, act_info->gpio_pin, GPIO_PIN_RESET);
		return true;
	}

	lmh__disable(LMH_A);
	lmh__disable(LMH_B);
	return false;

}

float lmh_get_att_VV(LMH_type type)
{
	return 20*log10(lmh_get_att_dB(type));
}

float lmh_get_att_dB(LMH_type type)
{
	LMH_info* act_info = &registered_lmh[type];
	return MAX_ATT_dB - (act_info->register_value[LMH6881SQ_ATTENUATION] * 0.25f);
}

uint8_t lmh_is_enabled(LMH_type type)
{
	LMH_info* act_info = &registered_lmh[type];
	return act_info->register_value[LMH6881SQ_POWER_DOWN] == TURN_ON_VALUE;
}

static void PGA_OFW_callback()
{
	lmh_diasble(LMH_A);
	lmh_diasble(LMH_B);
}

