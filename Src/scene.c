#include "scene.h"
#include "gfx.h"
#include "global_utils.h"
#include "task.h"
#include "stm32f7xx_hal.h"
#include "main_graph.h"
#include "Scene_control.h"
#include "FreeRTOS.h"

static Scene* scene = NULL;

static const Scene_control_info scene_controls_info[CONTROL_LAST] =
{
	/*TOP CONTROLS*/
	{.x = 0, 	.y = 0, 	.width = 200, 	.height = 60, 	.name = "MODE",		.def_val = " "},
	{.x = 200, 	.y = 0, 	.width = 200, 	.height = 60, 	.name = "STATUS",	.def_val = "INIT"},
	{.x = 400, 	.y = 0, 	.width = 200, 	.height = 60, 	.name = "LIMIT",	.def_val = " "},
	{.x = 600, 	.y = 0, 	.width = 200, 	.height = 60, 	.name = "PGA",		.def_val = " "},
	/*BOTTOM CONTROLS*/
	{.x = 0, 	.y = 540, 	.width = 160, 	.height = 60, 	.name = "OFFSET A",	.def_val = " "},
	{.x = 160, 	.y = 540, 	.width = 160, 	.height = 60, 	.name = "OFFSET B",	.def_val = " "},
	{.x = 320, 	.y = 540, 	.width = 160, 	.height = 60, 	.name = "DIV A",	.def_val = " "},
	{.x = 480, 	.y = 540, 	.width = 160, 	.height = 60, 	.name = "DIV B",	.def_val = " "},
	{.x = 640, 	.y = 540, 	.width = 160, 	.height = 60, 	.name = "TIME",		.def_val = " "},
	/*RIGHT CONTROLS*/
	{.x = 800, 	.y = 0, 	.width = 224, 	.height = 120, 	.name = "MENU",		.def_val = " "},
	{.x = 800, 	.y = 120, 	.width = 224, 	.height = 120, 	.name = "PROBE",	.def_val = "1x"},
	{.x = 800, 	.y = 240, 	.width = 224, 	.height = 120, 	.name = "UNIT",		.def_val = "V"},
	{.x = 800, 	.y = 360, 	.width = 224, 	.height = 120, 	.name = "CURSOR 1",	.def_val = "DISABLED"},
	{.x = 800, 	.y = 480, 	.width = 224, 	.height = 120, 	.name = "CURSOR 2",	.def_val = "DISABLED"}
};

/*
#define BUMP_BUFFER()				\
do {								\
	if(scene.act_buffer == 0)		\
		scene.act_buffer = 1;		\
	else							\
		scene.act_buffer = 0;		\
}while(0)

static void scene_swap_buffers()
{
	static LTDC_Layer_TypeDef* pLayReg = LTDC_Layer1;

	pixel_t* surface = gdispPixmapGetBits(scene.double_buffer[scene.act_buffer]);
	pLayReg->CFBAR = (uint32_t)surface & LTDC_LxCFBAR_CFBADD;
	LTDC->SRCR |= LTDC_SRCR_IMR;

}*/

void scene_deamon_handler(void* arg);

static void scene_redraw_all()
{
	//BUMP_BUFFER();
	//graph_set_display(scene.double_buffer[scene.act_buffer]);

	graph_redraw();
	for(uint8_t i = 0; i < CONTROL_LAST; ++i)
	{
		scene_control_redraw(&scene->controls[i]);
	}
	//scene_swap_buffers();
}

void scene_deamon_handler(void* arg);

int16_t scene_get_width()
{
	return gdispGetWidth();
}

Scene_control* scene_get_control(Scene_controls type)
{
	return &scene->controls[type];
}

void scene_set_ctrl_val(Scene_controls type, const char* val)
{
	Scene_control* ctrl = scene_get_control(type);
	scene_control_set_value(ctrl, val, true);
}

void scene_set_ctrl_backround(Scene_controls type, uint32_t color)
{
	Scene_control* ctrl = scene_get_control(type);
	scene_control_change_background_color(ctrl, color);
}

void scene_init()
{
	static uint8_t was_initialised = false;
	ASSERT(was_initialised == false);

	gdispSetOrientation(GDISP_ROTATE_180);

	was_initialised = true;
	scene = pvPortMalloc(sizeof(Scene));

	gwinSetDefaultFont(gdispOpenFont("UI2"));
	gwinSetDefaultStyle(&WhiteWidgetStyle, FALSE);

    scene->width = gdispGetWidth();
    scene->height = gdispGetHeight();

    //scene.act_buffer = 0;
	graph_init(GDISP, GRAPH_POS_X, GRAPH_POS_Y, GRAPH_WIDTH, GRAPH_HEIGHT);
	graph_set_middle_color(Yellow);

	for(uint8_t i = 0; i < CONTROL_LAST; ++i)
	{
		Scene_controls ctrl = (Scene_controls) i;
		const Scene_control_info* info = & scene_controls_info[ctrl];
		scene_control_init(&scene->controls[ctrl], info->x, info->y, info->width, info->height, GDISP);
		scene_control_set_name(&scene->controls[ctrl], info->name, false);
		scene_control_set_value(&scene->controls[ctrl], info->def_val, false);
		scene_control_enable_border(&scene->controls[ctrl]);
	}

	TaskHandle_t xHandle = NULL;
	ASSERT(xTaskCreate(scene_deamon_handler, "SCENE", 256, (void*)0, 0, &xHandle) == pdPASS);
	scene_redraw_all();
}

/****** TASKS ******/
void scene_deamon_handler(void* arg)
{
	//scene_redraw_all();
	while(1)
	{
	    vTaskDelay(20);
	    //scene_redraw_all();
	}
}
