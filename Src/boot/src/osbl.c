#include "osbl.h"
#include "global_utils.h"
#include "FreeRTOS.h"
#include "task.h"
#include "front_panel.h"
#include "dac.h"
#include "LMH6881SQ.h"
#include "gfx.h"
#include "PWM.h"
#include "ltdc.h"
#include "fmc.h"
#include "dma2D.h"
#include "SD.h"
#include "gwin/gwin.h"
#include "mf_font.h"
#include "fpga_communicator.h"
#include "scene.h"
#include "adc.h"
#include "Oscilloscope.h"

extern GDisplay* g_display;

static uint8_t _is_OS_turned_on = false;

TaskHandle_t t_resource_allocator;

void resource_allocator(void* argument)
{
	_is_OS_turned_on = true;

	adc_init();

	//lmh_init(LMH_B);
	fpga_communicator_init();
	front_panel_init();
	dac_init();
	//pclk_init();
	SD_init();
	gfxInit();
	scene_init();
	oscilloscope_run();

	while(1)
	{
		vTaskDelay(WAIT_FOR_EVER);
	}
}

void osbl()
{
	xTaskCreate(resource_allocator, "RES_ALLOC", 1024, (void*)0, 6, &t_resource_allocator);
	vTaskStartScheduler();
}


/*****INTERRUPTS*****/
extern void xPortSysTickHandler( void );
void SysTick_Handler(void)
{
	HAL_IncTick();
	if(_is_OS_turned_on)
		xPortSysTickHandler();
}
