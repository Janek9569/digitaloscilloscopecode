#include "memory_manager.h"
#include "FreeRTOSConfig.h"
#include "fmc.h"
#include "portable.h"
#include <stddef.h>
#include <stdint.h>

#define SDRAM_HEAP_OFFSET (1024UL*600UL*4UL)
#define SDRAM_START_HEAP_ADDR (FMC_START_ADDR + SDRAM_HEAP_OFFSET)
#define SDRAM_TOTAL_SIZE (0x1000000UL)
#define SDRAM_HEAP_SIZE (SDRAM_TOTAL_SIZE - SDRAM_HEAP_OFFSET)

static uint8_t internal_ram_heap[configMEMORY_MANAGER_INTERNAL_RAM_HEAP];

void memory_manager_init()
{
	const HeapRegion_t xHeapRegions[] =
	{
	    { ( uint8_t * ) &internal_ram_heap, configMEMORY_MANAGER_INTERNAL_RAM_HEAP },
	    { ( uint8_t * ) SDRAM_START_HEAP_ADDR, SDRAM_HEAP_SIZE },
	    { NULL, 0 }
	};

	vPortDefineHeapRegions( xHeapRegions );
}
